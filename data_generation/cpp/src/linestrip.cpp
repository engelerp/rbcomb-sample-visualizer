#include <linestrip.hpp>
#include <algorithm>
#ifndef NDEBUG
#include <iostream>
#endif

bool isEqual(const double x, const double y, const double tol=1e-8){
    return (x - y < tol) && (y - x < tol);
}

void reverseNodes(std::vector<double>& nodes){
    std::reverse(nodes.begin(), nodes.end());
    for(size_t i = 0; i < nodes.size()/2; ++i){
        std::swap(nodes[2*i], nodes[2*i+1]);
    }
}

LineStrip::LineStrip(std::vector<double> nodes): _nodes(nodes), _isClosed(false), _isActive(true) {}

void LineStrip::attach(LineStrip& other, int mode){
    #ifndef NDEBUG
    std::cout << "ATTACH\n";
    std::cout << "Mode: " << mode << std::endl;
    std::cout << "This: (" << _nodes[0] << ", " << _nodes[1] << ") ... (" << _nodes[_nodes.size()-2] << ", " << _nodes[_nodes.size()-1] << ")\n";
    std::cout << "Other: ";
    for(int i = 0; i < other._nodes.size()/2; i+=2){
        std::cout << "(" << other._nodes[2*i] << ", " << other._nodes[2*i+1] << "), ";
    } 
    std::cout << std::endl << std::endl;
    #endif
    if(mode == 1){ //normal attach
        _nodes.reserve(_nodes.size() + other._nodes.size()); //reserve
        _nodes.insert(_nodes.end(), other._nodes.begin()+2, other._nodes.end());
    }
    else if(mode == 2){ //reverse attach
        other._nodes.reserve(_nodes.size() + other._nodes.size()); //reserve
        other._nodes.insert(other._nodes.end(), _nodes.begin()+2, _nodes.end());
        _nodes.reserve(other._nodes.size());
        _nodes.clear();
        std::copy(other._nodes.begin(), other._nodes.end(), std::back_inserter(_nodes));
    }
    else if(mode == 3){ //end-to-end attach
        _nodes.reserve(_nodes.size() + other._nodes.size()); //reserve
        //std::reverse(other._nodes.begin(), other._nodes.end());
        reverseNodes(other._nodes);
        _nodes.insert(_nodes.end(), other._nodes.begin()+2, other._nodes.end());
    }
    else if(mode == 4){ //start-to-start attach
        _nodes.reserve(_nodes.size() + other._nodes.size()); //reserve
        //std::reverse(_nodes.begin(), _nodes.end());
        reverseNodes(_nodes);
        _nodes.insert(_nodes.end(), other._nodes.begin()+2, other._nodes.end());
    }
    else{ //the fuck are you even doing
        return;
    }
    if((_nodes[0] == _nodes[_nodes.size()-2]) && (_nodes[1] == _nodes[_nodes.size()-1])){
        _isClosed = true;
    }
}

void LineStrip::clear(){
    _nodes.clear();
    _isActive = false;
}

int LineStrip::canBeAttached(const LineStrip& other) const {
    bool normalAttach = isEqual(other._nodes[0], _nodes[_nodes.size()-2]) && isEqual(other._nodes[1],_nodes[_nodes.size()-1]);
    bool reverseAttach = isEqual(other._nodes[other._nodes.size()-2], _nodes[0]) && isEqual(other._nodes[other._nodes.size()-1],_nodes[1]);
    bool endToEndAttach = isEqual(other._nodes[other._nodes.size()-2], _nodes[_nodes.size()-2]) && isEqual(other._nodes[other._nodes.size()-1],_nodes[_nodes.size()-1]);
    bool startToStartAttach = isEqual(other._nodes[0], _nodes[0]) && isEqual(other._nodes[1],_nodes[1]);

    if(normalAttach && reverseAttach){
        return normalAttach;
    }
    else{
        return normalAttach + 2*reverseAttach + 3*endToEndAttach + 4*startToStartAttach;
    }
}

bool LineStrip::isActive() const {
    return _isActive;
}

bool LineStrip::isClosed() const {
    return _isClosed;
}

std::vector<double> LineStrip::nodes() const{
    return _nodes;
}

void LineStrip::closeLoop(){
    _nodes.push_back(_nodes[0]);
    _nodes.push_back(_nodes[1]);
    _isClosed = true;
}

std::string LineStrip::str() const{
    if(!_isActive){
        return "";
    }
    std::string returnStr = std::to_string(_nodes[0]);
    for(size_t i = 1; i < _nodes.size(); ++i){
        returnStr += " " + std::to_string(_nodes[i]);
    }

    return returnStr;
}