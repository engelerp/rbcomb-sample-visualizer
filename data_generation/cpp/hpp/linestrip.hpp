#pragma once
#include <vector>
#include <string>

class LineStrip{
public:
    LineStrip(std::vector<double> nodes);

    bool isClosed() const;
    bool isActive() const;
    int canBeAttached(const LineStrip& other) const;

    void attach(LineStrip& other, int mode);
    void clear();
    void closeLoop();

    std::vector<double> nodes() const;
    std::string str() const;

private:
    bool _isClosed;
    bool _isActive;
    std::vector<double> _nodes;
};