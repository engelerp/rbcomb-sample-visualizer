/*Takes a set of lines and organizes such that they form closed polygons*/

#include <linestrip.hpp>
#include <fstream>
#include <vector>
#include <iostream>

int main(){
    std::vector<LineStrip> linestrips;
    linestrips.reserve(273050); //this is the amount of linestrips expected


    std::string filepath = "../python/line_strips_top.txt";
    std::ifstream ifs(filepath);

    if(!ifs){
        std::cout << "Problem opening file." << std::endl;
    }

    int count;
    double bufs;
    std::vector<double> buf;
    buf.reserve(100);
    while(true){
        buf.clear();
        ifs >> count; //get number of nodes in next strip
        if(!ifs){
            break; //no more data to read
        }
        for(int i = 0; i < 2*count; ++i){
            ifs >> bufs;
            buf.push_back(bufs);
        }
        linestrips.emplace_back(buf);
    }

    std::cout << "Loaded " << linestrips.size() << " linestrips." << std::endl;

    int start = 0;
    int appends = 0;
    int polys_finished = 0;
    for(int iteration = 0; iteration < 500000; ++iteration){ //test with 10k iterations
        bool update_start = false;
        while((linestrips[start].isClosed() || !linestrips[start].isActive()) && (start < linestrips.size())){
            #ifndef NDEBUG
            std::cout << "Incrementing start,  start=" << start+1 << std::endl;
            #endif
            ++start;
            
            update_start = true;
        }
        if(start >= linestrips.size()){
            break;
        }
        if(update_start){
            polys_finished += 1;
            std::cout << "Finished " << polys_finished << " polygons" << std::endl;
        }
        bool change = false;
        for(int i = start+1; i < linestrips.size(); ++i){
            if(!linestrips[i].isActive() || linestrips[i].isClosed()){
                #ifndef NDEBUG
                //std::cout << "Skipping " << i << std::endl;
                #endif
                continue;
            }
            int mode = linestrips[start].canBeAttached(linestrips[i]);
            if(mode != 0){
                change = true;
                //std::cout << "Attaching " << i << " to " << start << ", mode " << mode << std::endl;
                //std::cout << "Size before insertion: " << linestrips[start].nodes().size() << std::endl;
                linestrips[start].attach(linestrips[i], mode);
                //std::cout << "Size after insertion: " << linestrips[start].nodes().size() << std::endl;
                linestrips[i].clear();
                ++appends;
                //std::cout << "Strip " << start << " length: " << linestrips[start].nodes().size() << std::endl;
                break;
            }
        }
        if(!change){
            //assume this loop is closed, move on
            linestrips[start].closeLoop();
        }
    }

    std::cout << "Done " << appends << " appends, start is " << start << std::endl;



    #ifndef NDEBUG //this only works if start is in range
    std::cout << "Current Linestrip start: (" << linestrips[start].nodes()[0] << " " << linestrips[start].nodes()[1] << ")" << std::endl;
    size_t size_n = linestrips[start].nodes().size();
    std::cout << "Current Linestrip end: (" << linestrips[start].nodes()[size_n-2] << " " << linestrips[start].nodes()[size_n-1] << ")" << std::endl;
    std::cout << "Last Linestrip:" << std::endl;
    for(auto d: linestrips[start].nodes()){
        std::cout << d << ", ";
    }
    std::cout << std::endl;
    
    std::cout << "Previous Linestrip:" << std::endl;
    while(!linestrips[--start].isClosed()){}
    for(auto d: linestrips[start].nodes()){
        std::cout << d << ", ";
    }
    std::cout << std::endl;
    #endif



    std::cout << "Preparing result string" << std::endl;
    std::string resultString = "";
    size_t num_polygons = 0;
    for(auto ls: linestrips){
        if(ls.isActive()){
            resultString += ls.str() + "\n";
            ++num_polygons;
        }
    }

    std::cout << "Writing " << num_polygons << " polygons to file" << std::endl;
    std::fstream outfile("../python/polygons_fixedarcs_top.txt", std::ios::out);
    outfile.write(resultString.c_str(), resultString.size());
    outfile.close();

    return 0;
}