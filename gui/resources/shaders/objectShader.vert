#version 410 core

layout (location = 0) in vec2 pos;
layout (location = 1) in vec2 col;
layout (location = 2) in float ind;
layout (location = 3) in float stat;

uniform float z_offset;

//these ints are represented as floats due to how the vertex array is organized
uniform float selected_index;
uniform vec4 color_selected;

//0: normal, 1: identification, 2: only good, 3: only bad, 4: only sketch, 5: good-bad-sketch, 6: normal-bad, 7: highlight selection
uniform int drawing_mode; 

uniform vec4 color_normal;
uniform vec4 color_good;
uniform vec4 color_bad;
uniform vec4 color_sketch;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

uniform float opacity;
uniform bool mirror;

out vec4 frag_color;

void main(){
    gl_Position = projection*view*model*vec4(pos[0], pos[1], z_offset, 1.);
    if(mirror){
        gl_Position = projection*view*model*vec4(-pos[0], pos[1], z_offset, 1.);
    }

    frag_color = color_normal;
    if(drawing_mode == 1){
        //identification
        frag_color = vec4(col[0], col[1], 0.f, 1.f);
    }
    else if(drawing_mode == 2){
        //only good
        if(stat > 0.5f){
            frag_color = vec4(0.f, 0.f, 0.f, 0.f);
        }
        else{
            frag_color = color_good;
        }
    }
    else if(drawing_mode == 3){
        //only bad
        if(stat < 1.5f){
            frag_color = vec4(0.f, 0.f, 0.f, 0.f);
        }
        else{
            frag_color = color_bad;
        }
    }
    else if(drawing_mode == 4){
        //only sketch
        if(stat < 0.5f || stat > 1.5f){
            frag_color = vec4(0.f, 0.f, 0.f, 0.f);
        }
        else{
            frag_color = color_sketch;
        }
    }
    else if(drawing_mode == 5){
        //good-bad-sketch
        if(stat < 0.5f){
            frag_color = color_good;
        }
        else if(stat < 1.5f){
            frag_color = color_sketch;
        }
        else{
            frag_color = color_bad;
        }
    }
    else if(drawing_mode == 6){
        //normal-bad
        if(stat > 1.5f){
            frag_color = color_bad;
        }
    }
    else if(drawing_mode == 7){
        frag_color.a = 0.1f;
    }
    if(selected_index == ind && drawing_mode != 1){
        frag_color = color_selected;
    }

    if(drawing_mode != 1 && (drawing_mode != 7 || opacity < 0.1f)){
        frag_color.a *= opacity;
    }
}