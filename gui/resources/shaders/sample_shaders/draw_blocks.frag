#version 410 core

precision highp float;

out vec4 FragColor;

in vec2 textureCoordinates;

uniform float color_multiplier;
uniform sampler2D source_texture;

void main()
{
	vec4 tex_color = texture(source_texture, textureCoordinates);

	FragColor = tex_color * color_multiplier;
}