#version 410 core

precision highp float;

out vec4 FragColor;

in vec2 textureCoordinates;
in vec2 downCoordinates;
in vec2 upCoordinates;
in vec2 leftCoordinates;
in vec2 rightCoordinates;
in float source;

uniform float c1;
uniform float c2;

uniform sampler2D tex_wave;
uniform sampler2D tex_damp;

void main(){
	vec3 onsite = texture(tex_wave, textureCoordinates).rgb;
	float down = texture(tex_wave, downCoordinates).r;
	float up = texture(tex_wave, upCoordinates).r;
	float left = texture(tex_wave, leftCoordinates).r;
	float right = texture(tex_wave, rightCoordinates).r;

	float damping = texture(tex_damp, textureCoordinates).r;

	vec4 sum = vec4(left, right, up, down);
	vec4 A = vec4(c1);
	float dp = dot(A, sum);
	//float newred = (dp + c2  * onsite.r - onsite.g) * damping;
	float newred = (dp + c2  * onsite.r - onsite.g) * damping + (1. - damping) * onsite.r;

	//FragColor = vec4(newred + onsite.b * source, onsite.rb, 1.);
  float update = mix(newred, source, onsite.b);
  FragColor = vec4(update, onsite.r, onsite.b, 1.);
}