#version 410 core
layout (location = 0) in vec3 verCoords;
layout (location = 1) in vec2 texCoords;

precision highp float;

uniform float dx;
uniform float dy;
uniform float c1;
uniform float c2;
uniform float t;
uniform float amplitude;
uniform float frequency;

out vec2 textureCoordinates;
out vec2 downCoordinates;
out vec2 upCoordinates;
out vec2 leftCoordinates;
out vec2 rightCoordinates;
out float source;

void main(){
	gl_Position = vec4(verCoords.x, verCoords.y, 0.0, 1.0);
	textureCoordinates = texCoords;
	downCoordinates = vec2(texCoords.x, texCoords.y - dy);
	upCoordinates = vec2(texCoords.x, texCoords.y + dy);
	leftCoordinates = vec2(texCoords.x - dx, texCoords.y);
	rightCoordinates = vec2(texCoords.x + dx, texCoords.y);
	source = amplitude * sin(frequency * t); //good choice: amplitude 0.3, frequency 10
}