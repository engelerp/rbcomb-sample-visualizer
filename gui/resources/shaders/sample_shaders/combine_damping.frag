#version 410 core

precision highp float;

out vec4 FragColor;

in vec2 textureCoordinates;

uniform sampler2D tex_damping_static; //static damping
uniform sampler2D tex_damping_dynamic; //dynamic damping

void main()
{
        float damping = min(texture(tex_damping_static, textureCoordinates).r, texture(tex_damping_dynamic, textureCoordinates).r);

        FragColor = vec4(damping, 0., 0., 1.);
}
