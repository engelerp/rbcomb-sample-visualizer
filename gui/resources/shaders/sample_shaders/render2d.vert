#version 410 core
layout (location = 0) in vec3 verCoords;
layout (location = 1) in vec2 texCoords;

precision highp float;

out vec2 textureCoordinates;

void main(){
float uv_offscreen = 0.2;
	gl_Position = vec4(verCoords.x, verCoords.y, 0.0, 1.0);
	//float uv_offscreen_corr = uv_offscreen / (1. + 2. * uv_offscreen);
	//textureCoordinates = vec2((1.0 - 2. * uv_offscreen) * texCoords.x + uv_offscreen, (1.0 - 2. * uv_offscreen) * texCoords.y + uv_offscreen);
	//textureCoordinates = vec2(texCoords.x, texCoords.y);
	//textureCoordinates = vec2((1.0 - 2. * uv_offscreen_corr) * texCoords.x + uv_offscreen_corr, (1.0 - 2. * uv_offscreen_corr) * texCoords.y + uv_offscreen_corr);
	textureCoordinates = vec2(texCoords.x, texCoords.y);
}