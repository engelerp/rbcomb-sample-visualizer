#version 410 core

precision highp float;

out vec4 FragColor;

in vec2 textureCoordinates;
in float z_coordin;

uniform sampler2D tex_wave; //wave
//uniform sampler2D tex_damp; //damping
uniform sampler2D tex_checkerboard; //checkerboard

void main(){
	vec4 color = texture(tex_checkerboard, textureCoordinates);
	FragColor = color;
}