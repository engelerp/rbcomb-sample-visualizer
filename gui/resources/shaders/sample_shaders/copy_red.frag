#version 410 core

precision highp float;

out vec4 FragColor;

in vec2 textureCoordinates;

uniform sampler2D texture1;
uniform sampler2D texture2;

void main()
{
        vec4 color = texture(texture1, textureCoordinates);
        float damping = texture(texture2, textureCoordinates).r;
        float damping_color = 1. - damping;
        FragColor = vec4((0.25*(1.+color.r)+0.25)*damping + damping_color*0.2, damping_color*0.2, damping_color*0.2, 1.);
}
