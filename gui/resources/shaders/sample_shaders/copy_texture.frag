#version 410 core

precision highp float;

out vec4 FragColor;

in vec2 textureCoordinates;

uniform sampler2D source_texture;

void main()
{
	FragColor = texture(source_texture, textureCoordinates);
}