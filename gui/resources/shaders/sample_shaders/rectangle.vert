#version 330 core
layout (location = 0) in vec3 aPos; //position at loc 0

out vec4 vertexColor; //color

uniform vec4 color;
uniform vec2 trans;

void main()
{
  gl_Position = vec4(aPos.x+trans.x, aPos.y+trans.y, aPos.z, 1.0);
  vertexColor = color;
}
