#version 410 core

out vec4 FragColor;

in vec2 uvcoords;

uniform sampler2D source_texture;

void main()
{
        vec4 colour = texture(source_texture, uvcoords);

        FragColor = vec4(colour.r, colour.g, colour.b, 1.);
}
