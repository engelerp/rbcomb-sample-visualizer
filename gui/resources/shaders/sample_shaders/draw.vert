#version 410 core
layout (location = 0) in vec3 verCoords;

uniform sampler2D source_texture;

out vec2 uvcoords;

void main()
{
        gl_Position = vec4(verCoords.x, verCoords.y, 0.0, 1.0);

        uvcoords = vec2((verCoords.x + 1.) * 0.5, (verCoords.y + 1.) * 0.5);
}
