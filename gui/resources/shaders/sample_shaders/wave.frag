#version 410 core
out vec4 FragColor;

in vec2 textureCoordinates;

uniform sampler2D waveTexture;
uniform sampler2D obstTexture;

void main()
{
        //FragColor = vec4(rgbValues,ourColor.w);
        //FragColor = mix(texture(waveTexture, textureCoordinates), texture(obstTexture, textureCoordinates), 0.2);
        float sampledWave = texture(waveTexture, textureCoordinates).r;
        float sampledDamp = texture(obstTexture, textureCoordinates).r;
        FragColor = vec4(sampledWave-(1. - sampledDamp), 0., 0., 1.);
        //FragColor = vec4(0.,1.,0.,1.);
}
