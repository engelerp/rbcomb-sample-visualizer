#version 410 core

precision highp float;

out vec4 FragColor;

in vec2 textureCoordinates;
in vec2 downCoordinates;
in vec2 upCoordinates;
in vec2 leftCoordinates;
in vec2 rightCoordinates;
in float source;


uniform float dx;
uniform float dy;
uniform float c1;
uniform float c2;
uniform float t;
uniform float amplitude;
uniform float frequency;


//idea:
//the newest state is in red, t-1 is in green
//source is in blue
//render from one texture to the other in each step
//swap in each step
//damping is kept in a seperate texture for dynamic updating from CPU

uniform sampler2D texture1; //other state
uniform sampler2D texture2; //damping

//red is position, green is velocity

void main()
{
        vec3 onsite = texture(texture1, textureCoordinates).rgb;
        float left = texture(texture1, leftCoordinates).r;
        float right = texture(texture1, rightCoordinates).r;
        float up = texture(texture1, upCoordinates).r;
        float down = texture(texture1, downCoordinates).r;

        float damping = texture(texture2, textureCoordinates).r;

        //trial optimizations
        vec4 sum = vec4(left,right,up,down);
        vec4 A = vec4(c1);
        float dp = dot(A,sum);
        //old timestep
        //float newred = (dp + c2*onsite.r - onsite.g) * damping;
        //new (sebi) timestep
        float newred = (dp + c2*onsite.r - onsite.g) * damping + (1. - damping) * onsite.r;

        //FragColor = vec4(newred + onsite.b*source, onsite.rb, 1.);
        //float update = mix(newred, source, 
        FragColor = vec4((1.-onsite.b)*newred + onsite.b*source, (1.-onsite.b)*onsite.r, onsite.b, 1.);
}
