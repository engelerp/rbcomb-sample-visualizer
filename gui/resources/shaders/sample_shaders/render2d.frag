#version 410 core

precision highp float;

out vec4 FragColor;

in vec2 textureCoordinates;

uniform sampler2D tex_wave; //wave
uniform sampler2D tex_damp; //damping
uniform sampler1D tex_palette; //colour palette

void main(){
	vec4 color = texture(tex_wave, textureCoordinates);
	float damping = texture(tex_damp, textureCoordinates).r;
	//Debugging: top line
	//Running: bottom line
	//float damping_color = (1. - damping)*60;
	float damping_color = (1. - damping);
	vec4 damp_color = vec4(0.2,  0.2, 0.2, 1.);
	vec4 palette_color = texture(tex_palette, 0.5 + color.r);
	vec4 final_color = damping_color * damp_color + (1. - damping_color) * palette_color;
	//FragColor = vec4((0.25*(1.+color.r)+0.25)*damping + damping_color*0.2, damping_color*0.2, damping_color*0.2, 1.);
	FragColor = final_color;
}