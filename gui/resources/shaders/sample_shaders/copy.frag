#version 410 core

precision mediump float;

out vec4 FragColor;

in vec2 textureCoordinates;

uniform sampler2D texture1;

void main()
{
        FragColor = texture(texture1, textureCoordinates);
}
