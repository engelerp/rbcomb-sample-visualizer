#version 410 core
layout (location = 0) in vec3 verCoords;
layout (location = 1) in vec2 texCoords;

precision highp float;

out vec2 textureCoordinates;
out float z_coordin;

uniform float uv_offscreen;
uniform mat4 trans;
uniform sampler2D tex_wave; //wave
//uniform sampler2D tex_damp; //damping
uniform sampler2D tex_checkerboard; //checkerboard

void main(){
	textureCoordinates = vec2((1.0 - 2. * uv_offscreen) * texCoords.s + uv_offscreen, (1.0 - 2. * uv_offscreen) * texCoords.t + uv_offscreen);
	float z_coordin = textureLod(tex_wave, textureCoordinates, 0.0).r;
	gl_Position = trans * vec4(verCoords.x, verCoords.y, z_coordin/10.0 , 1.0);
}