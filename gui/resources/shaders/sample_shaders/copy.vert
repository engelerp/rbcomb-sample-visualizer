#version 410 core
layout (location = 0) in vec3 verCoords;
layout (location = 1) in vec2 texCoords;

precision mediump float;

//uniform int width;
//uniform int height;

out vec2 textureCoordinates;

void main()
{
        gl_Position = vec4(verCoords.x, verCoords.y, 0.0, 1.0);
        textureCoordinates = texCoords;
}
