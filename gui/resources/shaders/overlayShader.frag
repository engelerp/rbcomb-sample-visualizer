#version 410 core

out vec4 FragColor;

in vec2 tex_coord;

uniform sampler2D imTexture;

void main(){
		FragColor = texture(imTexture, tex_coord);
		FragColor.a = 1.f;
}