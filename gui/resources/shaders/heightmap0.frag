#version 410 core
out vec4 FragColor;

in vec4 vertexColor;
in float colorCoord;
flat in int colorOverridden;
in vec3 normal;
in vec3 frag_pos;

uniform sampler1D colorscheme;

//Lighting
uniform int lighting_enabled;
uniform vec3 camera_pos;
uniform vec3 light_pos;

void main(){
  vec4 fragment_color;
  if(colorOverridden == 1){
    fragment_color = vertexColor;
  }
  else{
    fragment_color = texture(colorscheme, colorCoord);
  }

  if(lighting_enabled == 1){
    vec3 norm = normalize(normal);
    vec3 light_color = vec3(1.0, 1.0, 1.0); //White Light

    //ambient
    float ambientStrength = 0.1;
    vec3 ambient = ambientStrength * light_color;

    //diffuse
    vec3 light_dir = normalize(light_pos - frag_pos);
    float diff = max(dot(norm,light_dir), 0.0);
    vec3 diffuse = 0.9 * diff * light_color;

    //specular
    float specularStrength = 0.5;
    vec3 view_dir = normalize(camera_pos - frag_pos);
    vec3 reflect_dir = reflect(-light_dir, norm);
    float spec = pow(max(dot(view_dir, reflect_dir), 0.0), 32);
    vec3 specular = specularStrength * spec * light_color;


    FragColor = vec4((ambient + diffuse + specular), 1.0) * fragment_color;
  }
  else{
    FragColor = fragment_color;
  }


}
