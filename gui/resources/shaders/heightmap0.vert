#version 410 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;

out vec4 vertexColor;
out float colorCoord;
flat out int colorOverridden;
out vec3 normal;
out vec3 frag_pos;

//perspective related uniforms
uniform mat4 model;
uniform mat4 model_invt;
uniform mat4 view;
uniform mat4 projection;

//Scale related uniforms
uniform float scale;
uniform float minZ;
uniform float maxZ;
uniform float bad_z_value;

//Cross section plots lines related uniforms
uniform int plotLinesEnabled;
uniform vec3 plotLineAlongX_color;
uniform vec3 plotLineAlongY_color;
uniform float plotLineAlongX_y;
uniform float plotLineAlongY_x;
uniform float plotLineHalfThickness;

//Highlighting related uniforms
uniform int highlightingEnabled; //NEW
uniform vec2 highlightingPoint; //NEW
uniform float highlightingRadius; //NEW

//Leveling related uniforms
uniform mat4 modelSpaceRot;
uniform float originZ;

void main(){
	vec3 position = aPos;
  vec4 position_transformed = model * vec4(position, 1.0);
	gl_Position = projection * view * position_transformed;
	//colorCoord = (aPos.z - minZ)/(maxZ-minZ);
  vec4 pos_col = modelSpaceRot * (vec4(aPos, 1.) + vec4(0.0,0.0,-originZ,0.0)) + vec4(0.0,0.0,originZ,0.0);
  colorCoord = (pos_col.z - minZ)/(maxZ-minZ);
	vertexColor = vec4(position.z, 0.0, 0.0, 1.);
  colorOverridden = 0;

  //Check if on cross section line
  if(plotLinesEnabled == 1){
    if(abs(position.x - plotLineAlongY_x) < plotLineHalfThickness){
      colorOverridden = 1;
      vertexColor = vec4(plotLineAlongY_color, 1.);
    }
    else if(abs(position.y - plotLineAlongX_y) < plotLineHalfThickness){
      colorOverridden = 1;
      vertexColor = vec4(plotLineAlongX_color, 1.);
    }
  }
  //Check if highlighted
  if(highlightingEnabled == 1){
    vec4 highlightingColor = vec4(0.3f, 0.3f, 0.3f, 1.0f);
    if(distance(position_transformed.xy, highlightingPoint) < highlightingRadius){
      colorOverridden = 1;
      vertexColor = highlightingColor;
    }
  }

  normal = mat3(model_invt) * aNormal;
  frag_pos = vec3(model * vec4(aPos, 1.0));
}
