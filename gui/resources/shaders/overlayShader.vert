#version 410 core

layout (location = 0) in vec2 pos;
layout (location = 1) in vec2 texuv;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

uniform float opacity;
uniform float radians;
uniform vec2 offset;
uniform float scale;

out vec2 tex_coord;

void main(){
    vec2 pos_adjusted = scale * pos + offset;
    gl_Position = projection*view*model*vec4(pos_adjusted[0], pos_adjusted[1], 0.f, 1.f);
    tex_coord = texuv;
}