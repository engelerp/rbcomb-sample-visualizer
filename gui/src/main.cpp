#include <infrastructure.hpp>
#include <iostream>
#include <chrono>
#include <camera_arcball.hpp>
#include <layer.hpp>
#include <imgui_bundle.hpp>
#include <input_state.hpp>
#include <selection_manager.hpp>
#include <metadata_manager.hpp>
#include <visibility_manager.hpp>
#include <INIReader.h>

#include <screendefs.hpp>

//#define METADATA_PATH "C:\\Users\\Pascal\\repos\\rbcomb-sample-visualizer\\gui\\resources\\metadata\\"

int main(int argc, char** argv) {
	INIReader inireader("config.ini");
	if (inireader.ParseError() < 0) {
		std::cout << "Can't load config.ini" << std::endl;
		return 1;
	}
	std::string metadata_path = inireader.GetString("paths", "pathMetadata", "NONE");
	std::string models_path = inireader.GetString("paths", "pathModels", "NONE");
	std::string shaders_path = inireader.GetString("paths", "pathShaders", "NONE");
	std::string association_path = inireader.GetString("paths", "pathAssociation", "NONE");
	std::cout << "Config loaded:" << std::endl
						<< "pathMetadata = " << inireader.GetString("paths", "pathMetadata", "NONE") << std::endl
						<< "pathModels = " << inireader.GetString("paths", "pathModels", "NONE") << std::endl
						<< "pathShaders = " << inireader.GetString("paths", "pathShaders", "NONE") << std::endl
						<< "pathAssociation" << inireader.GetString("paths", "pathAssociation", "NONE") << std::endl;

	/*Init Window and OpenGL Context*/
	Infrastructure infra;
	infra.init("RBComb Sample Visualizer", WIDTH, HEIGHT);
	glEnable(GL_DEPTH_TEST);


	/*Init GUI*/
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImPlot::CreateContext();
	ImGui::StyleColorsDark();
	ImGui_ImplSDL2_InitForOpenGL(infra.window(), infra.context());
	const char* glsl_version = "#version 330";
	ImGui_ImplOpenGL3_Init(glsl_version);


	glm::vec3 lookAt(0., 0., 0.);
	glm::vec3 eye(0., 0., 130.1415);
	glm::vec3 upVector(0., 1., 0.);
	Camera camera (eye, lookAt, upVector);

	//Define materials
	Material mat_gold{ glm::vec4(255.f / 256.f, 215.f / 256.f, 0.f, 1.f), glm::vec4(0.f,1.f,0.f,1.f), glm::vec4(0.f,0.f,0.3f,1.f), glm::vec4(0.3f,0.f,0.0f,1.f) };
	Material mat_wafer{ glm::vec4(0.561f, 0.51f, 1.0f, 1.f), glm::vec4(0.f,1.f,0.f,1.f), glm::vec4(0.f,0.f,0.3f,1.f), glm::vec4(0.3f,0.f,0.0f,1.f) };
	Material mat_wafer_top{ glm::vec4(0.461f, 0.41f, 9.0f, 1.f), glm::vec4(0.f,1.f,0.f,1.f), glm::vec4(0.f,0.f,0.3f,1.f), glm::vec4(0.3f,0.f,0.0f,1.f) };
	Material mat_drum{ glm::vec4(0.804f, 0.51f, 1.0f, 0.9f), glm::vec4(0.f,0.85f,0.f,1.f), glm::vec4(0.f,0.f,0.15f,1.f), glm::vec4(0.15f,0.f,0.0f,1.f) };

	//Load layers
	//Layer bottom_metal ("Bottom Metal", "C:\\Users\\Pascal\\repos\\rbcomb-sample-visualizer\\data_generation\\python\\models\\bottom_metal_single\\bottom_metal.obj", 0.0f, mat_gold);
	//Layer bottom_drums ("Bottom Drums", "C:\\Users\\Pascal\\repos\\rbcomb-sample-visualizer\\data_generation\\python\\models\\bottom_drums_single\\bottom_drums.obj", 0.0f, mat_drum);
	//Layer bottom_silicon ("Bottom Silicon", "C:\\Users\\Pascal\\repos\\rbcomb-sample-visualizer\\data_generation\\python\\models\\bottom_silicon_single\\bottom_silicon.obj", 0.0f, mat_wafer);
	//Layer top_metal ("Top Metal", "C:\\Users\\Pascal\\repos\\rbcomb-sample-visualizer\\data_generation\\python\\models\\top_metal_single\\top_metal.obj", 0.0f, mat_gold);
	//Layer top_drums("Top Drums", "C:\\Users\\Pascal\\repos\\rbcomb-sample-visualizer\\data_generation\\python\\models\\top_drums_single\\top_drums.obj", 0.0f, mat_drum);
	//Layer top_silicon("Top Silicon", "C:\\Users\\Pascal\\repos\\rbcomb-sample-visualizer\\data_generation\\python\\models\\top_silicon_single\\top_silicon.obj", 0.0f, mat_wafer_top);
	Layer bottom_metal("Bottom Metal", models_path + "bottom_metal_single\\bottom_metal.obj", 0.0f, mat_gold);
	Layer bottom_drums("Bottom Drums", models_path + "bottom_drums_single\\bottom_drums.obj", 0.0f, mat_drum);
	Layer bottom_silicon("Bottom Silicon", models_path + "bottom_silicon_single\\bottom_silicon.obj", 0.0f, mat_wafer);
	Layer top_metal("Top Metal", models_path + "top_metal_single\\top_metal.obj", 0.0f, mat_gold);
	Layer top_drums("Top Drums", models_path + "top_drums_single\\top_drums.obj", 0.0f, mat_drum);
	Layer top_silicon("Top Silicon", models_path + "top_silicon_single\\top_silicon.obj", 0.0f, mat_wafer_top);
	MetadataManager::addLayer(bottom_metal);
	MetadataManager::markBottomMetal(bottom_metal.name());
	MetadataManager::addLayer(bottom_drums);
	MetadataManager::addLayer(top_metal);
	MetadataManager::addLayer(top_drums);

	std::cout << "Error Code: " << glGetError() << std::endl;
	std::cout << "GL_NO_ERROR: " << GL_NO_ERROR << std::endl;
	std::cout << "GL_INVALID_ENUM: " << GL_INVALID_ENUM << std::endl;
	std::cout << "GL_INVALID_VALUE: " << GL_INVALID_VALUE << std::endl;
	std::cout << "GL_INVALID_OPERATION: " << GL_INVALID_OPERATION << std::endl;
	std::cout << "GL_INVALID_FRAMEBUFFER_OPERATION: " << GL_INVALID_FRAMEBUFFER_OPERATION << std::endl;
	std::cout << "GL_OUT_OF_MEMORY: " << GL_OUT_OF_MEMORY << std::endl;
	std::cout << "GL_MAX_TEXTURE_SIZE: " << GL_MAX_TEXTURE_SIZE << std::endl;

	
	glm::mat4 projection = glm::perspective(glm::radians(45.f), static_cast<float>(WIDTH) / static_cast<float>(HEIGHT), 0.1f, 200.0f);
	glm::mat4 view = camera.GetViewMatrix();
	glm::mat4 model = glm::mat4(1.0f);

	int draw_mode = 0;

	bool top_mirrored = false;

	size_t count = 0;
	auto start = std::chrono::high_resolution_clock::now();
	while (!InputState::should_quit) {
		++count; //number of frames processed

		InputState::update(WIDTH, HEIGHT);
		if (InputState::rmb_pressed) {
#ifndef NDEBUG
			std::cout << "Panning" << std::endl;
#endif
			camera.ProcessMousePan(-InputState::mmot_x, InputState::mmot_y);
		}
		else if (InputState::esc_click) {
			//reset view and selection
			camera.SetCameraView(eye, lookAt, upVector);
		}
		else if (InputState::g_click) {
			if (SelectionManager::selectedIndex != -1) {
				MetadataManager::getData(SelectionManager::selectedLayerName, SelectionManager::selectedIndex).status = 0;
				//Update layer data here, and push to GPU
				if (SelectionManager::selectedLayerName == bottom_metal.name()) {
					bottom_metal.setObjectState(SelectionManager::selectedIndex, 0);
				}
				else if (SelectionManager::selectedLayerName == bottom_drums.name()) {
					bottom_drums.setObjectState(SelectionManager::selectedIndex, 0);
				}
				else if (SelectionManager::selectedLayerName == top_metal.name()) {
					top_metal.setObjectState(SelectionManager::selectedIndex, 0);
				}
				else if (SelectionManager::selectedLayerName == top_drums.name()) {
					top_drums.setObjectState(SelectionManager::selectedIndex, 0);
				}
			}
		}
		else if (InputState::s_click) {
			if (SelectionManager::selectedIndex != -1) {
				MetadataManager::getData(SelectionManager::selectedLayerName, SelectionManager::selectedIndex).status = 1;
				//Update layer data here, and push to GPU
				if (SelectionManager::selectedLayerName == bottom_metal.name()) {
					bottom_metal.setObjectState(SelectionManager::selectedIndex, 1);
				}
				else if (SelectionManager::selectedLayerName == bottom_drums.name()) {
					bottom_drums.setObjectState(SelectionManager::selectedIndex, 1);
				}
				else if (SelectionManager::selectedLayerName == top_metal.name()) {
					top_metal.setObjectState(SelectionManager::selectedIndex, 1);
				}
				else if (SelectionManager::selectedLayerName == top_drums.name()) {
					top_drums.setObjectState(SelectionManager::selectedIndex, 1);
				}
			}
		}
		else if (InputState::b_click) {
			if (SelectionManager::selectedIndex != -1) {
				MetadataManager::getData(SelectionManager::selectedLayerName, SelectionManager::selectedIndex).status = 2;
				//Update layer data here, and push to GPU
				if (SelectionManager::selectedLayerName == bottom_metal.name()) {
					bottom_metal.setObjectState(SelectionManager::selectedIndex, 2);
				}
				else if (SelectionManager::selectedLayerName == bottom_drums.name()) {
					bottom_drums.setObjectState(SelectionManager::selectedIndex, 2);
				}
				else if (SelectionManager::selectedLayerName == top_metal.name()) {
					top_metal.setObjectState(SelectionManager::selectedIndex, 2);
				}
				else if (SelectionManager::selectedLayerName == top_drums.name()) {
					top_drums.setObjectState(SelectionManager::selectedIndex, 2);
				}
			}
		}
		else if (InputState::t_click) {
			VisibilityManager::enableTop = !VisibilityManager::enableTop;
		}
		else if (InputState::lmb_click) {
			//select object
			int selected_index = VisibilityManager::enableTop ? top_metal.selectObject(InputState::umpos_x, HEIGHT - InputState::umpos_y, projection, view, model) : -1;
			if (selected_index != -1) {
				SelectionManager::selectedIndex = selected_index;
				SelectionManager::selectedLayerName = top_metal.name();
			}
			else {
				selected_index = VisibilityManager::enableTop ? top_drums.selectObject(InputState::umpos_x, HEIGHT - InputState::umpos_y, projection, view, model) : -1;
				if (selected_index != -1) {
					SelectionManager::selectedIndex = selected_index;
					SelectionManager::selectedLayerName = top_drums.name();
				}
				else{
					selected_index = VisibilityManager::enableBottom ? bottom_metal.selectObject(InputState::umpos_x, HEIGHT - InputState::umpos_y, projection, view, model) : -1;
					if (selected_index != -1) {
						SelectionManager::selectedIndex = selected_index;
						SelectionManager::selectedLayerName = bottom_metal.name();
					}
					else {
						selected_index = VisibilityManager::enableBottom ? bottom_drums.selectObject(InputState::umpos_x, HEIGHT - InputState::umpos_y, projection, view, model) : -1;
						if (selected_index != -1) {
							SelectionManager::selectedIndex = selected_index;
							SelectionManager::selectedLayerName = bottom_drums.name();
						}
						else {
							SelectionManager::selectedIndex = -1;
							SelectionManager::selectedLayerName = "";
						}
					}
				}
			}
#ifndef NDEBUG
			std::cout << "Selected object: " << SelectionManager::selectedIndex << std::endl;
#endif
		}
		else if (InputState::mwheelmot_y != 0.f) {
#ifndef NDEBUG
			std::cout << "Zooming" << std::endl;
#endif
			std::cout << "Zooming " << InputState::mwheelmot_y << std::endl;
			//calculate position of mouse cursor in real world
			glm::vec2 m_real_world = camera.GetWorldXyFromMouse(InputState::mpos_x, InputState::mpos_y);
			//move camera to position pointed to by cursor
			camera.MoveTo(m_real_world);
			//move mouse to screen center (doesn't visually work in remote desktop)
			SDL_WarpMouseInWindow(infra.window(), WIDTH/2, HEIGHT/2);
			//process zoom (keep only this for central zoom)
			camera.ProcessMouseZoom(InputState::mwheelmot_y);
		}
		else if (InputState::f1_click) {
			//zoom 25
			//calculate position of mouse cursor in real world
			glm::vec2 m_real_world = camera.GetWorldXyFromMouse(InputState::mpos_x, InputState::mpos_y);
			//reset view and selection
			camera.SetCameraView(eye, lookAt, upVector);
			//move camera to position pointed to by cursor
			camera.MoveTo(m_real_world);
			//move mouse to screen center (doesn't visually work in remote desktop)
			SDL_WarpMouseInWindow(infra.window(), WIDTH / 2, HEIGHT / 2);
			//zoom to desired level
			for (int i = 0; i < 25; ++i) {
				camera.ProcessMouseZoom(1);
			}
		}
		else if (InputState::f2_click) {
			//zoom 40
			//calculate position of mouse cursor in real world
			glm::vec2 m_real_world = camera.GetWorldXyFromMouse(InputState::mpos_x, InputState::mpos_y);
			//reset view and selection
			camera.SetCameraView(eye, lookAt, upVector);
			//move camera to position pointed to by cursor
			camera.MoveTo(m_real_world);
			//move mouse to screen center (doesn't visually work in remote desktop)
			SDL_WarpMouseInWindow(infra.window(), WIDTH / 2, HEIGHT / 2);
			//zoom to desired level
			for (int i = 0; i < 40; ++i) {
				camera.ProcessMouseZoom(1);
			}
		}
		else if (InputState::f3_click) {
			//zoom 50
			//calculate position of mouse cursor in real world
			glm::vec2 m_real_world = camera.GetWorldXyFromMouse(InputState::mpos_x, InputState::mpos_y);
			//reset view and selection
			camera.SetCameraView(eye, lookAt, upVector);
			//move camera to position pointed to by cursor
			camera.MoveTo(m_real_world);
			//move mouse to screen center (doesn't visually work in remote desktop)
			SDL_WarpMouseInWindow(infra.window(), WIDTH / 2, HEIGHT / 2);
			//zoom to desired level
			for (int i = 0; i < 50; ++i) {
				camera.ProcessMouseZoom(1);
			}
		}
		SelectionManager::hoveredIndex = VisibilityManager::enableTop ? top_metal.selectObject(InputState::umpos_x, HEIGHT - InputState::umpos_y, projection, view, model) : -1;
		if (SelectionManager::hoveredIndex != -1) {
			SelectionManager::hoveredLayerName = top_metal.name();
		}
		else {
			SelectionManager::hoveredIndex = VisibilityManager::enableTop ? top_drums.selectObject(InputState::umpos_x, HEIGHT - InputState::umpos_y, projection, view, model) : -1;
			if (SelectionManager::hoveredIndex != -1) {
				SelectionManager::hoveredLayerName = top_drums.name();
			}
			else {
				SelectionManager::hoveredIndex = VisibilityManager::enableBottom ? bottom_metal.selectObject(InputState::umpos_x, HEIGHT - InputState::umpos_y, projection, view, model) : -1;
				if (SelectionManager::hoveredIndex != -1) {
					SelectionManager::hoveredLayerName = bottom_metal.name();
				}
				else {
					SelectionManager::hoveredIndex = VisibilityManager::enableBottom ? bottom_drums.selectObject(InputState::umpos_x, HEIGHT - InputState::umpos_y, projection, view, model) : -1;
					if (SelectionManager::hoveredIndex != -1) {
						SelectionManager::hoveredLayerName = bottom_drums.name();
					}
				}
			}
		}

		/*New Gui Frame*/
		ImGui_ImplOpenGL3_NewFrame();
		ImGui_ImplSDL2_NewFrame(infra.window());
		ImGui::NewFrame();

		/*Hover Tooltip*/
		if (SelectionManager::hoveredIndex != -1) {
			ImGui::SetNextWindowBgAlpha(0.5);
			ImGui::BeginTooltip();
			std::string tool_tip_str = "Layer: " + SelectionManager::hoveredLayerName + "\n";
			tool_tip_str += "Index: " + std::to_string(SelectionManager::hoveredIndex) + "\n";
			tool_tip_str += "Status: ";
			switch (MetadataManager::getData(SelectionManager::hoveredLayerName, SelectionManager::hoveredIndex).status) {
			case 0:
				tool_tip_str += "good\n";
				break;
			case 1:
				tool_tip_str += "sketch\n";
				break;
			case 2:
				tool_tip_str += "broken\n";
				break;
			default:
				tool_tip_str += "unknown (" + std::to_string(MetadataManager::getData(SelectionManager::hoveredLayerName, SelectionManager::hoveredIndex).status) + ")\n";
				break;
			}

			ImGui::Text(tool_tip_str.c_str());
			ImGui::EndTooltip();
		}


		/*ImGui Windows*/
		//Info Window
		ImGui::Begin("Info");
		std::string text = "Selected Layer: " + SelectionManager::selectedLayerName;
		ImGui::Text(text.c_str());
		text = "Selected Object: " + std::to_string(SelectionManager::selectedIndex);
		ImGui::Text(text.c_str());
		if (SelectionManager::selectedIndex != -1) {
			ObjectMetadata metaDataObj = MetadataManager::getData(SelectionManager::selectedLayerName, SelectionManager::selectedIndex);
			ImGui::Text("Assignment:");
			ImGui::Text(metaDataObj.info.c_str());
			ImGui::NewLine();
			int object_status = metaDataObj.status;
			text = "Status: ";
			ImGui::Text(text.c_str());
			ImGui::SameLine();
			ImGui::RadioButton("Good", &object_status, 0);
			ImGui::SameLine();
			ImGui::RadioButton("Sketch", &object_status, 1);
			ImGui::SameLine();
			ImGui::RadioButton("Broken", &object_status, 2);
			if (object_status != MetadataManager::getData(SelectionManager::selectedLayerName, SelectionManager::selectedIndex).status) {
				MetadataManager::getData(SelectionManager::selectedLayerName, SelectionManager::selectedIndex).status = object_status;
				//Update layer data here, and push to GPU
				if (SelectionManager::selectedLayerName == bottom_metal.name()) {
					bottom_metal.setObjectState(SelectionManager::selectedIndex, object_status);
				}
				else if (SelectionManager::selectedLayerName == bottom_drums.name()) {
					bottom_drums.setObjectState(SelectionManager::selectedIndex, object_status);
				}
				else if (SelectionManager::selectedLayerName == top_metal.name()) {
					top_metal.setObjectState(SelectionManager::selectedIndex, object_status);
				}
				else if (SelectionManager::selectedLayerName == top_drums.name()) {
					top_drums.setObjectState(SelectionManager::selectedIndex, object_status);
				}
			}
		}
		ImGui::NewLine();
		ImGui::Separator();
		ImGui::NewLine();
		glm::vec2 m_real_world = camera.GetWorldXyFromMouse(InputState::mpos_x, InputState::mpos_y);
		std::ostringstream xstr;
		xstr << std::fixed << m_real_world.x;
		std::ostringstream ystr;
		ystr << std::fixed << m_real_world.y;
		text = "Mouse Position: (" + xstr.str() + ", " + ystr.str() + ")";
		ImGui::Text(text.c_str());
		ImGui::End();

		//Layer Window
		ImGui::Begin("Layers");

		//BOTTOM METAL
		text = "Layer: " + bottom_metal.name();
		ImGui::Text(text.c_str());
		text = "Objects: " + std::to_string(bottom_metal.numObjects());
		ImGui::Text(text.c_str());
		static char input_text[256] = "";
		ImGui::InputText("Metadata File", input_text, IM_ARRAYSIZE(input_text));
		if (ImGui::Button("Save")) {
			std::string path = metadata_path + std::string(input_text);
			MetadataManager::save(path, bottom_metal.name());
		}
		ImGui::SameLine();
		if (ImGui::Button("Load")) {
			std::string path = metadata_path + std::string(input_text);
			//load data
			MetadataManager::load(path, bottom_metal.name());
			//push data to GPU
			bottom_metal.setAllObjectStates(MetadataManager::getLayerData(bottom_metal.name()));
		}

		ImGui::NewLine();
		ImGui::Separator();
		ImGui::NewLine();

		//BOTTOM DRUMS
		text = "Layer: " + bottom_drums.name();
		ImGui::Text(text.c_str());
		text = "Objects: " + std::to_string(bottom_drums.numObjects());
		ImGui::Text(text.c_str());
		static char input_text2[256] = "";
		ImGui::InputText("Metadata File ", input_text2, IM_ARRAYSIZE(input_text2));
		if (ImGui::Button("Save ")) {
			std::string path = metadata_path + std::string(input_text2);
			MetadataManager::save(path, bottom_drums.name());
		}
		ImGui::SameLine();
		if (ImGui::Button("Load ")) {
			std::string path = metadata_path + std::string(input_text2);
			MetadataManager::load(path, bottom_drums.name());
			//push data to GPU
			bottom_drums.setAllObjectStates(MetadataManager::getLayerData(bottom_drums.name()));
		}

		ImGui::NewLine();
		ImGui::Separator();
		ImGui::NewLine();

		//TOP METAL
		text = "Layer: " + top_metal.name();
		ImGui::Text(text.c_str());
		text = "Objects: " + std::to_string(top_metal.numObjects());
		ImGui::Text(text.c_str());
		static char input_text3[256] = "";
		ImGui::InputText("Metadata File  ", input_text3, IM_ARRAYSIZE(input_text3));
		if (ImGui::Button("Save  ")) {
			std::string path = metadata_path + std::string(input_text3);
			MetadataManager::save(path, top_metal.name());
		}
		ImGui::SameLine();
		if (ImGui::Button("Load  ")) {
			std::string path = metadata_path + std::string(input_text3);
			MetadataManager::load(path, top_metal.name());
			//push data to GPU
			top_metal.setAllObjectStates(MetadataManager::getLayerData(top_metal.name()));
		}

		ImGui::NewLine();
		ImGui::Separator();
		ImGui::NewLine();
		
		//TOP DRUMS
		text = "Layer: " + top_drums.name();
		ImGui::Text(text.c_str());
		text = "Objects: " + std::to_string(top_drums.numObjects());
		ImGui::Text(text.c_str());
		static char input_text4[256] = "";
		ImGui::InputText("Metadata File   ", input_text4, IM_ARRAYSIZE(input_text4));
		if (ImGui::Button("Save   ")) {
			std::string path = metadata_path + std::string(input_text4);
			MetadataManager::save(path, top_drums.name());
		}
		ImGui::SameLine();
		if (ImGui::Button("Load   ")) {
			std::string path = metadata_path + std::string(input_text4);
			MetadataManager::load(path, top_drums.name());
			//push data to GPU
			top_drums.setAllObjectStates(MetadataManager::getLayerData(top_drums.name()));
		}
		
		ImGui::NewLine();
		ImGui::Separator();
		ImGui::Separator();
		ImGui::NewLine();
		ImGui::Text("Controls");
		ImGui::NewLine();
		ImGui::Text("Bottom                     Top");
		ImGui::SliderFloat("Opacity Mixer", &VisibilityManager::opacityMixer, 0.f, 1.f);
		ImGui::Checkbox("Mirror Top", &top_mirrored);
		top_metal.setMirrorState(top_mirrored);
		top_drums.setMirrorState(top_mirrored);
		top_silicon.setMirrorState(top_mirrored);
		ImGui::Checkbox("Enable Top", &VisibilityManager::enableTop);
		ImGui::SameLine();
		ImGui::Checkbox("Enable Bottom", &VisibilityManager::enableBottom);
		ImGui::End();

		//Draw Window
		ImGui::Begin("Draw");
		ImGui::RadioButton("Realistic", &draw_mode, 0);
		ImGui::SameLine();
		ImGui::RadioButton("Identification", &draw_mode, 1);
		ImGui::RadioButton("Good", &draw_mode, 2);
		ImGui::SameLine();
		ImGui::RadioButton("Bad", &draw_mode, 3);
		ImGui::SameLine();
		ImGui::RadioButton("Sketch", &draw_mode, 4);
		ImGui::RadioButton("Good-Bad-Sketch", &draw_mode, 5);
		ImGui::SameLine();
		ImGui::RadioButton("Normal-Bad", &draw_mode, 6);
		ImGui::RadioButton("Highlight", &draw_mode, 7);
		ImGui::End();

		glClearColor(78./255, 97./255., 117./255., 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		/*Update projection, view, model*/
		view = camera.GetViewMatrix();


		/*Draw here*/
		//Bottom Stack
		if (VisibilityManager::enableBottom) {
			//Bottom Wafer
			if (InputState::space_pressed) { //highlight selection
				int tempDrawingMode = 7;
				bottom_silicon.draw(projection, view, model, tempDrawingMode, -1, VisibilityManager::bottomOpacity());
			}
			else {
				bottom_silicon.draw(projection, view, model, 0, -1, VisibilityManager::bottomOpacity());
			}
			//Bottom Drum layer
			int selected_index = -1;
			if (SelectionManager::selectedLayerName == bottom_drums.name()) {
				selected_index = SelectionManager::selectedIndex;
			}
			if (InputState::space_pressed) { //highlight selection
				int tempDrawingMode = 7;
				bottom_drums.draw(projection, view, model, tempDrawingMode, selected_index, VisibilityManager::bottomOpacity());
			}
			else {
				bottom_drums.draw(projection, view, model, draw_mode, selected_index, VisibilityManager::bottomOpacity());
			}
			//Bottom Metal layer
			selected_index = -1;
			if (SelectionManager::selectedLayerName == bottom_metal.name()) {
				selected_index = SelectionManager::selectedIndex;
			}
			if (InputState::space_pressed) { //highlight selection
				int tempDrawingMode = 7;
				bottom_metal.draw(projection, view, model, tempDrawingMode, selected_index, VisibilityManager::bottomOpacity());
			}
			else {
				bottom_metal.draw(projection, view, model, draw_mode, selected_index, VisibilityManager::bottomOpacity());
			}
		}
		
		//Top Stack
		if (VisibilityManager::enableTop) {
			//Top Drum layer
			int selected_index = -1;
			if (SelectionManager::selectedLayerName == top_drums.name()) {
				selected_index = SelectionManager::selectedIndex;
			}
			if (InputState::space_pressed) { //highlight selection
				int tempDrawingMode = 7;
				top_drums.draw(projection, view, model, tempDrawingMode, selected_index, VisibilityManager::topOpacity());
			}
			else {
				top_drums.draw(projection, view, model, draw_mode, selected_index, VisibilityManager::topOpacity());
			}
			//Top Metal layer
			selected_index = -1;
			if (SelectionManager::selectedLayerName == top_metal.name()) {
				selected_index = SelectionManager::selectedIndex;
			}
			if (InputState::space_pressed) { //highlight selection
				int tempDrawingMode = 7;
				top_metal.draw(projection, view, model, tempDrawingMode, selected_index, VisibilityManager::topOpacity());
			}
			else {
				top_metal.draw(projection, view, model, draw_mode, selected_index, VisibilityManager::topOpacity());
			}
			//Top Wafer
			if (InputState::space_pressed) { //highlight selection
				int tempDrawingMode = 7;
				top_silicon.draw(projection, view, model, tempDrawingMode, -1, VisibilityManager::topOpacity());
			}
			else {
				top_silicon.draw(projection, view, model, 0, -1, VisibilityManager::topOpacity());
			}
			//Top Metal layer, draw on top if not mirrored
			if (!top_mirrored) {
				selected_index = -1;
				if (SelectionManager::selectedLayerName == top_metal.name()) {
					selected_index = SelectionManager::selectedIndex;
				}
				if (InputState::space_pressed) { //highlight selection
					int tempDrawingMode = 7;
					top_metal.draw(projection, view, model, tempDrawingMode, selected_index, VisibilityManager::topOpacity());
				}
				else {
					top_metal.draw(projection, view, model, draw_mode, selected_index, VisibilityManager::topOpacity());
				}
			}
		}
		




		/*End GUI Frame*/
		ImGui::Render();
		glViewport(0, 0, WIDTH, HEIGHT);
		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
		SDL_GL_SwapWindow(infra.window());


		auto end = std::chrono::high_resolution_clock::now();
		auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
		if (duration > 1000) {
			std::cout << "FPS: " << count << std::endl << std::flush;
			count = 0;
			start = std::chrono::high_resolution_clock::now();
		}
	}

	return 0;
}
