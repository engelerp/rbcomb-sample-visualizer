#include <visibility_manager.hpp>
#include <algorithm>

bool VisibilityManager::enableTop = true;
bool VisibilityManager::enableBottom = true;

float VisibilityManager::opacityMixer = 0.5;

float VisibilityManager::topOpacity() {
	return std::min(2.f * opacityMixer, 1.f);
}
float VisibilityManager::bottomOpacity() {
	return std::min(2.f * (1.f - opacityMixer), 1.f);
}