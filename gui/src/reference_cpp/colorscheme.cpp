#include <colorscheme.hpp>
#include <fstream>
#include <vector>
#include <iostream>
#include <utility.hpp>

Colorscheme::Colorscheme(std::string filename) : _filename(filename) {
	/*Load file*/
	_colors.reserve(300);
	try {
		std::ifstream file(filename);
		float color;
		while (file >> color) {
			_colors.push_back(color);
		}
		file.close();
	}
	catch (const std::exception e) {
		throw Nt1100_exception("Colorscheme::Colorscheme: Failed to load from file " + filename + ": " + e.what());
	}
	//sanity check loaded data
	auto invalid_color_it = std::find_if(_colors.begin(), _colors.end(), [](auto c) {return c < 0.f || c > 1.f; });

	if (_colors.size() < 4 || (_colors.size() / 4) * 4 != _colors.size() || invalid_color_it != _colors.end()) {
		throw Nt1100_exception("Colorscheme::Colorscheme: Data loaded from " + filename + " failed sanity check");
	}

	/*Upload texture to GPU*/
	//Generate Texture
	glGenTextures(1, &_texture);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_1D, _texture);
	glTexImage1D(GL_TEXTURE_1D, 0, GL_RGBA32F, _colors.size() / 4, 0, GL_RGBA, GL_FLOAT, _colors.data());
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glBindTexture(GL_TEXTURE_1D, 0);
}

bool Colorscheme::is(const std::string filename) const {
	return filename == _filename;
}

GLuint Colorscheme::getTexture() const {
	return _texture;
}

float Colorscheme::color2value(const float r, const float g, const float b) const {
#ifndef NDEBUG
	std::cout << "Color Requested: (" << r << ", " << g << ", " << b << ")" << std::endl;
	for (int i = 0; i < 7; ++i) {
		std::cout << "_colors[" << i << "] = (" << _colors[4 * i] << ", " << _colors[4 * i + 1] << ", " << _colors[4 * i + 2] << "), unequal: ";
		if (r != _colors[4 * i]) {
			std::cout << "r, " << r << " != " << _colors[4 * i];
		}
		if (g != _colors[4 * i+1]) {
			std::cout << "; g, " << g << " != " << _colors[4 * i + 1];
		}
		if (b != _colors[4 * i+2]) {
			std::cout << "b, " << b << " != " << _colors[4 * i + 2];
		}
		std::cout << std::endl;
	}
#endif
	float tol = 1e-6;
	size_t i_found = 0;
	for (; 4 * i_found < _colors.size(); ++i_found) {
		if (r == _colors[4 * i_found] && g == _colors[4 * i_found + 1] && b == _colors[4 * i_found + 2]) {
			break;
		}
	}
	if (4 * i_found < _colors.size()) {
		return static_cast<float>(4 * i_found) / _colors.size();
	}
	else {
		return -1.f;
	}
}