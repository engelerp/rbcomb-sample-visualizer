#include <resource_manager.h>
#include <algorithm>
#include <exception>
#include <utility.hpp>

std::vector<std::shared_ptr<Heightmap>> ResourceManager::_heightmaps;
std::vector<std::shared_ptr<Shader>> ResourceManager::_shaders;
std::vector<std::shared_ptr<Colorscheme>> ResourceManager::_colorschemes;

std::shared_ptr<Heightmap> ResourceManager::fetch_heightmap(const std::string filename) {
	/*Check if Heightmap is already loaded*/
	auto found = std::find_if(_heightmaps.begin(), _heightmaps.end(), [filename](auto h) {return h->is(filename); });
	if (found != _heightmaps.end()) {
		return *found;
	}
	else {
		try {
			std::cout << "Constructing Heightmap " << filename << std::endl << std::flush;
			_heightmaps.push_back(std::make_shared<Heightmap>(filename));
			std::cout << "Heightmap constructed." << std::endl << std::flush;
			return _heightmaps.back();
		}
		catch(std::exception e){
			std::cerr << e.what() << std::endl;
			throw Nt1100_exception("ResourceManager::fetch_heightmap(" + filename + "): Failed");
		}
	}
}

std::shared_ptr<Shader> ResourceManager::fetch_shader(const std::string vertexPath, const std::string fragmentPath) {
	/*Check if Shader is already loaded*/
	/*TODO: For the moment, this is disabled, because not all uniforms are applied per drawcall, there is some heightmap-state stored in them.*/
	/*
	auto found = std::find_if(_shaders.begin(), _shaders.end(), [vertexPath, fragmentPath](auto s) {return s->is(vertexPath, fragmentPath); });
	*/
	auto found = _shaders.end();
	if (found != _shaders.end()) {
		return *found;
	}
	else {
		try {
			_shaders.push_back(std::make_shared<Shader>(vertexPath.c_str(), fragmentPath.c_str()));
			return _shaders.back();
		}
		catch (std::exception e) {
			std::cerr << e.what() << std::endl;
			throw Nt1100_exception("ResourceManager::fetch_shader(" + vertexPath + ", " + fragmentPath + "): Failed");
		}
	}
}

std::shared_ptr<Colorscheme> ResourceManager::fetch_colorscheme(std::string filename) {
	auto found = std::find_if(_colorschemes.begin(), _colorschemes.end(), [filename](auto c) {return c->is(filename); });
	if (found != _colorschemes.end()) {
		return *found;
	}
	else {
		try {
			_colorschemes.push_back(std::make_shared<Colorscheme>(filename));
			return _colorschemes.back();
		}
		catch (std::exception e) {
			std::cerr << e.what() << std::endl;
			throw Nt1100_exception("ResourceManager::fetch_colorscheme(" + filename + ") Failed");
		}
	}
}