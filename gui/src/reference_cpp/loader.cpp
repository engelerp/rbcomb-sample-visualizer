#include <loader.hpp>
#include <imgui_bundle.hpp>
#include <string>

std::shared_ptr<Heightmap> Loader::_heightmap;
char Loader::_buf[128] = "";

bool Loader::load_update() {
	bool load_successful = false;
	ImGui::Begin("Heightmap Loader", nullptr);
	ImGui::InputText("Filename", _buf, 128);
	ImGui::SameLine();
	if (ImGui::Button("Load")) {
		//Load heightmap here
		try {
			_heightmap = ResourceManager::fetch_heightmap(std::string(_buf));
			load_successful = true;
		}
		catch (...) {
			std::cerr << "Loader::load_update: Failed to load heightmap from " << std::string(_buf) << ":\n"  << std::endl;
			return false;
		}
	}
	ImGui::End();
	return load_successful;
}

std::shared_ptr<Heightmap> Loader::getHeightmap() {
	return _heightmap;
}