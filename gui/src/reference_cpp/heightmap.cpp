#include <heightmap.hpp>
#include <utility.hpp>

#include <exception>
#include <fstream>
#include <iostream>
#include <sstream>
#include <utility>
#include <algorithm>
#include <resource_manager.h>
#include <imgui_bundle.hpp>

Heightmap::Heightmap(std::string filename): _filename(filename) {
	std::string file_string = "";
	//Load file contents into file_string
	try {
		std::ifstream file(filename);
		std::stringstream buffer;
		buffer << file.rdbuf();
		file_string = buffer.str();
		file.close();
	}
	catch(const std::exception e){
		std::cerr << "An error occurred loading file " << filename << ": " << e.what() << std::endl;
		file_string = "";
	}

	if (file_string != "") {
		//if intensity file, read up to intensity data
		size_t eof_index = file_string.find("Intensity");

		if (eof_index == std::string::npos) {
			//no intensity file, we read till end
			eof_index = file_string.size();
		}

		//find start of data
		size_t start = file_string.find("\nOPD\t");
		size_t end = file_string.find("\n", start + 1);

		size_t datastart = end + 1;

		//count lines
		end++;
		size_t num_lines = std::count(file_string.begin() + end, file_string.end(), '\n');

		//extract data into _data
		_data.reserve(num_lines * 3);
		while (datastart < eof_index - 1) {
			std::array<double, 3> temp_arr = extract_triplet(file_string, datastart);
			_data.insert(_data.end(), temp_arr.begin(), temp_arr.end());
		}

		_minX = min(_data, 0, 3);
		_maxX = max(_data, 0, 3);
		_minY = min(_data, 1, 3);
		_maxY = max(_data, 1, 3);
		_minZ = min(_data, 2, 3);
		_maxZ = max(_data, 2, 3);
		_zRangeUm = (_maxZ - _minZ) * 0.08; //in um
		_zRangeUmOffset = 0.f;
		std::cout << "Identified z range: " << _zRangeUm << " um (" << _minZ << " - " << _maxZ << ")" << std::endl;
		std::cout << "Unprocessed z range: " << _maxZ << " - " << _minZ << " (delta=" << _maxZ - _minZ << ")" << std::endl;
		/*Throw if _zRange is less than 1 A (i.e. it's probably 0)*/
		if (_zRangeUm < 1e-4) {
			throw Nt1100_exception("Heightmap::Heightmap(std::string): z range is too close to zero (" + std::to_string(_zRangeUm) + ")");
		}
		/*TODO: Renormalize model coordinates sensible here*/
		//For now, I just renormalize the z range to [0,1]
		double zRangeInverse = 1. / (_maxZ - _minZ);
		for (size_t i = 2; i < _data.size(); i += 3) {
			if (_data[i] >= _minZ) {
				_data[i] -= _minZ;
				_data[i] *= zRangeInverse;
			}
		}
		//recalculate _minZ, maxZ
		_minZ = min(_data, 2, 3); //should be 0
		_maxZ = max(_data, 2, 3); //should be 1

		std::cout << "Identified z range " << _minZ << " - " << _maxZ << std::endl;

		/*Initialize Renderer Infrastructure*/
		glGenBuffers(1, &_vbo);
		glGenBuffers(1, &_ebo);
		glGenVertexArrays(1, &_vao);

		/*Calculate EBO indexing array*/
		std::vector<unsigned> ebo_array;
		ebo_array.reserve(2 * _data.size()); //rough upper bound
		//Calculate Ny assuming that _data is crossed column by column
		_Ny = 0;
		while (3 * _Ny < _data.size() && _data[3 * _Ny] == _data[0]) {
			++_Ny;
		}
		//Sanity check _Ny
		if (_Ny < 5 || 3 * _Ny + 10 > _data.size()) {
			throw Nt1100_exception("Heightmap::Heightmap(std::string): Probably failed to deduce _Ny. Found _Ny=" + std::to_string(_Ny) + ", for _data of size " + std::to_string(_data.size()));
		}
		//Deduce _Nx
		_Nx = _data.size() / 3 / _Ny;
		if (3 * _Nx * _Ny != _data.size()) {
			throw Nt1100_exception("Heightmap::Heightmap(std::string): Value of _Nx=" + std::to_string(_Nx) + " or _Ny=" + std::to_string(_Ny) + " is incorrect for _data of size " + std::to_string(_data.size()));
		}
		//Calculate EBO indices and normals
		std::vector<std::array<float, 3>> normals(_data.size()/3, { {0.f, 0.f, 0.f} });
		for (unsigned i = 0; i < _Nx - 1; ++i) {
			for (unsigned j = 0; j < _Ny - 1; ++j) {
				std::array<std::pair<unsigned, unsigned>, 6> ijs = { { { i, j }, {i, j + 1}, {i + 1, j}, {i + 1, j}, {i + 1, j + 1}, {i, j + 1} } };
				std::array<unsigned, 6> index_array;
				std::transform(ijs.begin(), ijs.end(), index_array.begin(), [&](const std::pair<unsigned, unsigned>& p) {return ij2index(p.first, p.second, _Ny); });
				//Only insert triangle if all points are good
				auto bad_z_it = std::find_if(index_array.begin(), index_array.begin() + 3, [&](auto ind) {return _data[3 * ind + 2] == BAD_Z_VALUE; });
				if (bad_z_it == index_array.begin() + 3) {
					ebo_array.insert(ebo_array.end(), index_array.begin(), index_array.begin() + 3);
				}
				bad_z_it = std::find_if(index_array.begin() + 3, index_array.end(), [&](auto ind) {return _data[3 * ind + 2] == BAD_Z_VALUE; });
				if (bad_z_it == index_array.end()) {
					ebo_array.insert(ebo_array.end(), index_array.begin() + 3, index_array.end());
				}
				//Calculate normals either way
				std::array<float, 3> p1 = { {_data[3 * index_array[0]], _data[3 * index_array[0] + 1], _data[3 * index_array[0] + 2]} };
				std::array<float, 3> p2 = { {_data[3 * index_array[2]], _data[3 * index_array[2] + 1], _data[3 * index_array[2] + 2]} };
				std::array<float, 3> p3 = { {_data[3 * index_array[1]], _data[3 * index_array[1] + 1], _data[3 * index_array[1] + 2]} };
				std::array<float, 3> p4 = { {_data[3 * index_array[4]], _data[3 * index_array[4] + 1], _data[3 * index_array[4] + 2]} };
				std::array<float, 3> p5 = { {_data[3 * index_array[5]], _data[3 * index_array[5] + 1], _data[3 * index_array[5] + 2]} };
				std::array<float, 3> p6 = { {_data[3 * index_array[3]], _data[3 * index_array[3] + 1], _data[3 * index_array[3] + 2]} };
				auto n1 = normal(p1, p2, p3);
				normals[index_array[0]] += n1;
				normals[index_array[1]] += n1;
				normals[index_array[2]] += n1;
				auto n2 = normal(p4, p5, p6);
				normals[index_array[3]] += n2;
				normals[index_array[4]] += n2;
				normals[index_array[5]] += n2;
			}
		}
		//normalize normals
		std::transform(normals.begin(), normals.end(), normals.begin(), [](std::array<float, 3> v) {return v /= length(v); });
		_numElements = ebo_array.size();
		std::cout << "Loaded model contains " << _numElements / 3 << " triangles" << std::endl << std::flush;
		/*Upload EBO data*/
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ebo);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned) * ebo_array.size(), ebo_array.data(), GL_STATIC_DRAW);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		/*Upload VBO data*/
		//Temporarily cast to float
		std::vector<float> data_complete_float;
		data_complete_float.reserve(2*_data.size());
		for (size_t i = 0; i < normals.size(); ++i) {
			data_complete_float.push_back(_data[3 * i]);
			data_complete_float.push_back(_data[3 * i + 1]);
			data_complete_float.push_back(_data[3 * i + 2]);
			data_complete_float.push_back(normals[i][0]);
			data_complete_float.push_back(normals[i][1]);
			data_complete_float.push_back(normals[i][2]);
		}
		glBindBuffer(GL_ARRAY_BUFFER, _vbo);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float) * data_complete_float.size(), data_complete_float.data(), GL_STATIC_DRAW);
		/*Setup VAO*/
		glBindVertexArray(_vao);
		glBindBuffer(GL_ARRAY_BUFFER, _vbo);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ebo);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)));
		glEnableVertexAttribArray(1);
		glBindVertexArray(0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		/*Load colorscheme*/
		_colorscheme_ptr = ResourceManager::fetch_colorscheme(std::string(COLORSCHEME_LOC) + "viridis.csc");
		/*Setup shader*/
		/*TODO: This should be made prettier*/
		std::string vertFile = std::string(SHADER_LOC) + std::string("heightmap0.vert");
		std::string fragFile = std::string(SHADER_LOC) + std::string("heightmap0.frag");
		_shader_ptr = ResourceManager::fetch_shader(vertFile, fragFile);
		_scale = 0.5f;
		_modelSpaceRot = glm::mat4(1.f);
		_highlightingRadius = 0.05f;
		_shader_ptr->use();
		_shader_ptr->setFloat("scale", _scale);
		_shader_ptr->setFloat("minZ", static_cast<float>(_minZ));
		_shader_ptr->setFloat("maxZ", static_cast<float>(_maxZ));
		_shader_ptr->setFloat("bad_z_value", static_cast<float>(BAD_Z_VALUE));
		_shader_ptr->setInt("colorscheme", 0);
    _shader_ptr->setInt("plotLinesEnabled", 1);
    _shader_ptr->setVec3("plotLineAlongX_color", glm::vec3(1.f,0.f,1.f));
    _shader_ptr->setVec3("plotLineAlongY_color", glm::vec3(1.f,0.f,0.f));
    _shader_ptr->setFloat("plotLineHalfThickness", 0.001f);
    _shader_ptr->setFloat("plotLineAlongX_y", -99999.f);
    _shader_ptr->setFloat("plotLineAlongY_x", -99999.f);
		_shader_ptr->setVec3("light_pos", glm::vec3(0.f, 0.f, 50.f));
		_shader_ptr->setInt("lighting_enabled", 1);
		_shader_ptr->setFloat("highlightingRadius", _highlightingRadius);
		_shader_ptr->setInt("highlightingEnabled", 0);
		_shader_ptr->setMat4("modelSpaceRot", _modelSpaceRot);
		_shader_ptr->setFloat("originZ", _data[2]);
		_shader_ptr->unuse();

		/*Setup Framebuffer*/
		_framebuffer_ptr = std::make_shared<Framebuffer>();

    /*Setup plotting vectors*/
    _plotAlongX_y = _minY + (_maxY-_minY)/2.f;
    _plotAlongY_x = _minX + (_maxX-_minX)/2.f;
    _last_plotAlongX_y = -99999.f;
    _last_plotAlongY_x = -99999.f;
    _plotAlongX_data_x.resize(_Nx);
    _plotAlongX_data_h.resize(_Nx);
    _plotAlongY_data_y.resize(_Ny);
    _plotAlongY_data_h.resize(_Ny);
		_plot_gets_focus = false;
		_plot_gets_collapse = false;
	}
	else {
		throw Nt1100_exception("Heightmap::Heightmap(std::string): File " + filename + " contents are empty.");
	}
}

void Heightmap::draw(glm::mat4 projection, glm::mat4 view, glm::mat4 model, glm::vec3 camera_pos){
	_shader_ptr->use();
	_shader_ptr->setMat4("projection", projection);
	_shader_ptr->setMat4("view", view);
	_shader_ptr->setMat4("model", model);
	glm::mat4 model_invt(model[0], model[1], model[2], glm::vec4(0,0,0,1));
	model_invt = glm::transpose(glm::inverse(model_invt));
	_shader_ptr->setMat4("model_invt", model_invt);
	_shader_ptr->setVec3("camera_pos", camera_pos);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_1D, _colorscheme_ptr->getTexture());
	glBindVertexArray(_vao);
	glBindBuffer(GL_ARRAY_BUFFER, _vbo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ebo);
	glDrawElements(GL_TRIANGLES, _numElements, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	_shader_ptr->unuse();
}

void Heightmap::upload() {
	return;
}

void Heightmap::unload() {
	return;
}

bool Heightmap::is(const std::string filename) const {
	return _filename == filename;
}

float Heightmap::getSurfaceHeight(unsigned x, unsigned y, glm::mat4 projection, glm::mat4 view, glm::mat4 model) {
	_framebuffer_ptr->bind();

	glClearColor(0.f, 0.f, 0.f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  //temporarily disable plot lines for this drawcall
  _shader_ptr->use();
  _shader_ptr->setInt("plotLinesEnabled", 0);
	_shader_ptr->setInt("lighting_enabled", 0);
	_shader_ptr->setInt("highlightingEnabled", 0);
  _shader_ptr->unuse();

	draw(projection, view, model, glm::vec3(0,0,0)); //camera position irrelevant without lighting

  //re-enable plot lines
  _shader_ptr->use();
  _shader_ptr->setInt("plotLinesEnabled", 1);
	_shader_ptr->setInt("lighting_enabled", 1);
	_shader_ptr->setInt("highlightingEnabled", 1);
  _shader_ptr->unuse();

	auto color = _framebuffer_ptr->getPixel(x, y);
	float defo_um = _colorscheme_ptr->color2value(color[0], color[1], color[2]);

	_framebuffer_ptr->unbind();

	if (defo_um != -1.f) {
		_plot_gets_focus = true;
		return defo_um * _zRangeUm - _zRangeUmOffset;
	}
	else {
		_plot_gets_collapse = true;
		return NOT_ON_SURFACE;
	}
}

float Heightmap::getAverageSurfaceHeight(glm::vec2 xy) {
	float total_height = 0.f;
	unsigned num_values = 0;
	float rad_sq = _highlightingRadius * _highlightingRadius;
	for (size_t i = 0; i < _data.size() / 3; ++i) {
		if (std::pow(_data[3 * i] - xy[0], 2) + std::pow(_data[3 * i + 1] - xy[1], 2) < rad_sq) {
			if (_data[3 * i + 2] != BAD_Z_VALUE) {
				total_height += _data[3 * i + 2];
				++num_values;
			}
		}
	}
	if (num_values != 0) {
		return total_height / static_cast<float>(num_values);
	}
	else {
		return -1.f;
	}
}

void Heightmap::updatePlotWindow(){
	size_t slashpos = _filename.rfind("\\");
	std::string drumname = _filename.substr(slashpos + 1, _filename.length() - slashpos - 1);
	std::string title = "Height Profile " + drumname + "##" + _filename;
	ImGui::SetNextWindowSize(ImVec2(450, 690));
	ImGui::SetNextWindowPos(ImVec2(10, 10));
	if (_plot_gets_focus) {
		ImGui::SetNextWindowCollapsed(false);
		ImGui::SetNextWindowFocus();
		_plot_gets_focus = false;
	}
	if (_plot_gets_collapse) {
		ImGui::SetNextWindowCollapsed(true);
		_plot_gets_collapse = false;
	}
	ImGui::Begin(title.c_str(), nullptr);
	std::string labelstr = "Offset y##" + _filename;
	ImGui::SliderFloat(labelstr.c_str(), &_plotAlongX_y, _minY, _maxY, "%.3f mm");
	labelstr = "Offset x##" + _filename;
	ImGui::SliderFloat(labelstr.c_str(), &_plotAlongY_x, _minX, _maxX, "%.3f mm");
  //update uniforms accordingly
  _shader_ptr->use();
  _shader_ptr->setFloat("plotLineAlongX_y", _plotAlongX_y);
  _shader_ptr->setFloat("plotLineAlongY_x", _plotAlongY_x);
  _shader_ptr->unuse();
	_plotHeightLineAlongX(_plotAlongX_y);
	_plotHeightLineAlongY(_plotAlongY_x);
	ImGui::End();
}

float Heightmap::minZ() const{
  return _minZ;
}

float Heightmap::minX() const {
	return _minX;
}

float Heightmap::maxX() const {
	return _maxX;
}

float Heightmap::minY() const {
	return _minY;
}

float Heightmap::maxY() const {
	return _maxY;
}

float Heightmap::zRangeUm() const {
	return _zRangeUm;
}

void Heightmap::setZRangeUmOffset(float offset) {
	_zRangeUmOffset += offset;
}

void Heightmap::setZRangeUmFromModelZ(float z_model) {
	_zRangeUmOffset = z_model * _zRangeUm;
}

//Shader control
void Heightmap::enableLighting() {
	_shader_ptr->use();
	_shader_ptr->setInt("lighting_enabled", 1);
	_shader_ptr->unuse();
}

void Heightmap::disableLighting() {
	_shader_ptr->use();
	_shader_ptr->setInt("lighting_enabled", 0);
	_shader_ptr->unuse();
}


void Heightmap::enableHighlighting(glm::vec2 hpos) {
	_shader_ptr->use();
	_shader_ptr->setInt("highlightingEnabled", 1);
	_shader_ptr->setVec2("highlightingPoint", hpos);
	_shader_ptr->unuse();
}
void Heightmap::disableHighlighting() {
	_shader_ptr->use();
	_shader_ptr->setInt("highlightingEnabled", 0);
	_shader_ptr->unuse();
}

void Heightmap::calculateNewZRange(glm::mat4 rot) {
	float maxiZ = -99999.f;
	float miniZ = 99999.f;
	for (size_t i = 0; i < _data.size() / 3; ++i) {
		if (_data[3 * i + 2] == BAD_Z_VALUE) {
			continue;
		}
		else {
			glm::vec4 newP = rot * glm::vec4(_data[3 * i], _data[3 * i + 1], _data[3 * i + 2], 1.f);
			if (newP.z > maxiZ) {
				maxiZ = newP.z;
			}
			if (newP.z < miniZ) {
				miniZ = newP.z;
			}
		}
	}
	_maxZ = maxiZ;
	_minZ = miniZ;
	_shader_ptr->use();
	_shader_ptr->setFloat("minZ", static_cast<float>(_minZ));
	_shader_ptr->setFloat("maxZ", static_cast<float>(_maxZ));
	_shader_ptr->unuse();
}

void Heightmap::setModelspaceRot(glm::mat4 rot) {
	_last_plotAlongX_y = -1.f; //force recalculation of plot
	_last_plotAlongY_x = -1.f; //force recalculation of plot
	_modelSpaceRot = rot * _modelSpaceRot;
	_shader_ptr->use();
	_shader_ptr->setMat4("modelSpaceRot", _modelSpaceRot);
	_shader_ptr->unuse();
}

void Heightmap::_plotHeightLineAlongX(const float y){
	if(y != _last_plotAlongX_y){
		_last_plotAlongX_y = y;
		if(y > _maxY || y < _minY){
			return;
		}
		//setup translation vector
		float z_offset = _zRangeUmOffset / _zRangeUm;
		//find correct i (row)
		//TODO: This has to change when _data changes
		size_t stride = 3;
		size_t i = 0;
		for(; i < _Ny; ++i){
			if(_data[stride*i + 1] >= y){
				break;
			}
		}
		if(i >= _Ny){
			std::cout << "_plotHeightLineAlongX: Data not found." << std::endl;
			return;
		}
		std::cout << "_plotHeightLineAlongX: Found element " << i << ", y = " << _data[stride * i + 1] << std::endl;
		//traverse the row, push xs and zs to the plotting vectors
		for(size_t j = 0; j < _Nx; ++j){
			_plotAlongX_data_x[j] = _data[stride * j * _Ny + stride * i];
			if (_data[stride * j * _Ny + stride * i + 2] != BAD_Z_VALUE) {
				//transform data
				float x = _data[stride * j * _Ny + stride * i];
				float y = _data[stride * j * _Ny + stride * i + 1];
				float z = _data[stride * j * _Ny + stride * i + 2];
				glm::vec4 data(x, y, z - z_offset, 1.);
				data = _modelSpaceRot * data;
				float transformed_z = data.z + z_offset;
				_plotAlongX_data_h[j] = _zRangeUm * (transformed_z - _minZ) / (_maxZ - _minZ) - _zRangeUmOffset;
			}
			else {
				_plotAlongX_data_h[j] = NAN;
			}
		}
	}
	//plot data
	if(ImPlot::BeginPlot("Heightplot along x")){
		ImPlot::SetupAxes("Position [mm]", "Height [um]");
		ImPlot::SetNextLineStyle(ImVec4(1.f,0.f,1.f,1.f));
		ImPlot::PlotLine("Height along x", _plotAlongX_data_x.data(), _plotAlongX_data_h.data(), _Nx);
		ImPlot::EndPlot();
	}
}

void Heightmap::_plotHeightLineAlongY(const float x){
	if(x != _last_plotAlongY_x){
		_last_plotAlongY_x = x;
		if(x > _maxX || x < _minX){
			return;
		}
		//setup translation vector
		float z_offset = _zRangeUmOffset / _zRangeUm;
		//find correct i (column)
		//TODO: This has to change when _data changes
		size_t stride = 3;
		size_t i = 0;
		for(; i < _Nx; ++i){
			if(_data[stride*_Ny*i] >= x){
				break;
			}
		}
		if(i >= _Nx){
			std::cout << "_plotHeightLineAlongY: Data not found." << std::endl;
			return;
		}
		std::cout << "_plotHeightLineAlongY: Found element " << i << ", x = " << _data[stride * _Ny * i] << std::endl;
		//traverse the column, push ys and zs to the plotting vectors
		for(size_t j = 0; j < _Ny; ++j){
			_plotAlongY_data_y[j] = _data[stride * _Ny * i + stride * j + 1];
			if (_data[stride * _Ny * i + stride * j + 2] != BAD_Z_VALUE) {
				//transform data
				float x = _data[stride * _Ny * i + stride * j];
				float y = _data[stride * _Ny * i + stride * j + 1];
				float z = _data[stride * _Ny * i + stride * j + 2];
				glm::vec4 data(x, y, z - z_offset, 1.);
				data = _modelSpaceRot * data;
				float transformed_z = data.z + z_offset;
				_plotAlongY_data_h[j] = _zRangeUm * (transformed_z - _minZ) / (_maxZ - _minZ) - _zRangeUmOffset;
			}
			else {
				_plotAlongY_data_h[j] = NAN;
			}
		}
	}
	//plot data
	if(ImPlot::BeginPlot("Heightplot along y")){
		ImPlot::SetupAxes("Position [mm]", "Height [um]");
		ImPlot::SetNextLineStyle(ImVec4(1.f,0.f,0.f,1.f));
		ImPlot::PlotLine("Height along y", _plotAlongY_data_y.data(), _plotAlongY_data_h.data(), _Ny);
		ImPlot::EndPlot();
	}
}
