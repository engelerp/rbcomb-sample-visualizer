#include <utility.hpp>


Nt1100_exception::Nt1100_exception(std::string what): _what(what) {}

const char* Nt1100_exception::what() const {
	return _what.c_str();
}

std::array<double, 3> extract_triplet(const std::string& str_in, size_t& start) {
	size_t eol_i = str_in.find('\n', start + 1);
	size_t locstart = start;
	start = eol_i + 1; //start for next line
	//first number
	size_t end1 = str_in.find('\t', locstart + 1);
	//second number
	size_t end2 = str_in.find('\t', end1 + 1);
	//third number
	size_t end3 = str_in.find('\n', end2 + 1);

	std::array<double, 3> ret_arr;
	try {
		ret_arr[0] = std::stod(str_in.substr(locstart, end1 - locstart));
		ret_arr[1] = std::stod(str_in.substr(end1, end2 - end1));
	}
	catch (std::exception& e) {
		std::cout << "utility.cpp: extract_triplet: stod failed: " << e.what() << std::endl;
		throw e;
	}
	try {
		double extract = std::stod(str_in.substr(end2, end3 - end2)); //might throw if conversion fails
		ret_arr[2] = extract;
	}
	catch (const std::exception& e) {
		ret_arr[2] = BAD_Z_VALUE;
	}
	return ret_arr;
}


unsigned ij2index(unsigned i, unsigned j, unsigned Ny) {
	return i * Ny + j;
}


std::array<float, 3> operator+=(std::array<float, 3>& lhs, const std::array<float, 3>& rhs){
  lhs[0] += rhs[0];
  lhs[1] += rhs[1];
  lhs[2] += rhs[2];

  return lhs;
}

std::array<float, 3> operator-(std::array<float, 3> lhs, const std::array<float, 3>& rhs) {
	lhs[0] -= rhs[0];
	lhs[1] -= rhs[1];
	lhs[2] -= rhs[2];
	return lhs;
}

std::array<float, 3> cross(const std::array<float, 3>& v, const std::array<float, 3>& w){
  std::array<float, 3> cp;
  cp[0] = v[1]*w[2] - v[2]*w[1];
  cp[1] = v[2]*w[0] - v[0]*w[2];
  cp[2] = v[0]*w[1] - v[1]*w[0];

  return cp;
}

std::array<float, 3> operator/=(std::array<float, 3>& v, const float d){
  v[0] /= d;
  v[1] /= d;
  v[2] /= d;

  return v;
}

float length(const std::array<float, 3>& v) {
	return std::sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
}

std::array<float, 3> normal(const std::array<float, 3>& p1, const std::array<float, 3>& p2, const std::array<float, 3>& p3) {
	std::array<float, 3> v1 = p2 - p1;
	std::array<float, 3> v2 = p3 - p1;
	std::array<float, 3> crs = cross(v1, v2);
	crs /= length(crs);
	return crs;
}

glm::mat4 scale_z(float z0, float factor){
  glm::mat4 transf (1.f);
  transf = glm::translate(glm::scale(glm::translate(transf, glm::vec3(0.f,0.f,-z0)), glm::vec3(1.,1.,factor)), glm::vec3(0.,0.,z0));
  return transf;
}
