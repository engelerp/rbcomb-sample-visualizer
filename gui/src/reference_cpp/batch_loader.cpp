#include <batch_loader.hpp>
#include <imgui_bundle.hpp>
#include <string>
#include <resource_manager.h>
#include <filesystem>

std::vector<std::shared_ptr<Heightmap>> BatchLoader::_heightmaps;
char BatchLoader::_buf[128] = "drum1.asc";
char BatchLoader::_basePath[128] = "C:\\Users\\Pascal\\repos\\nt1100-analyser\\resources\\datasets\\";

bool BatchLoader::load_update() {
	bool load_complete = false;
	ImGui::Begin("Heightmap Batch Loader", nullptr);
	ImGui::InputText("Base Path", _basePath, 128);
	ImGui::InputText("Drum Name", _buf, 128);
	ImGui::SameLine();
	if(ImGui::Button("Load")) {
		/*Clear Heightmap vector*/
		_heightmaps.clear();
		/*Construct base path*/
		std::filesystem::path bp(_basePath);
		if (!bp.has_filename()) {
			/*Iterate over all subdirectories*/
			for (const auto& entry : std::filesystem::recursive_directory_iterator(bp)) {
				/*In each subdirectory, try loading the drum and pushing to the Heightmap vector*/
				if (entry.path().filename() == std::string(_buf)) {
					try {
						auto heightmap = ResourceManager::fetch_heightmap(entry.path().string());
						_heightmaps.push_back(heightmap);
						load_complete = true;
					}
					catch(...) {
						std::cout << "BatchLoader: Failed to load Heightmap " << entry.path().string() << std::endl;
					}
				}
			}
		}
	}
	ImGui::End();
	return load_complete;
}

std::vector<std::shared_ptr<Heightmap>> BatchLoader::getHeightmaps() {
	return _heightmaps;
}