#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <exception>
#include <array>
#include <algorithm>
#include <vector>

static int bad_lines = 0;

std::array<double, 3> extract_triplet(const std::string& str_in, size_t& start) {
	size_t eol_i = str_in.find('\n', start + 1);
	size_t locstart = start;
	start = eol_i + 1; //start for next line
	//first number
	size_t end1 = str_in.find('\t', locstart + 1);
	//second number
	size_t end2 = str_in.find('\t', end1 + 1);
	//third number
	size_t end3 = str_in.find('\n', end2 + 1);

	std::array<double, 3> ret_arr;
	ret_arr[0] = std::stod(str_in.substr(locstart, end1 - locstart));
	ret_arr[1] = std::stod(str_in.substr(end1, end2 - end1));
	try {
		double extract = std::stod(str_in.substr(end2, end3 - end2));
		ret_arr[2] = extract;
	}
	catch (const std::exception& e){
		++bad_lines;
		//std::cerr << "Encountered line with only two elements (" << ret_arr[0] << ", " << ret_arr[1] << ")" << std::endl;
		ret_arr[2] = -999999.;
	}
	return ret_arr;
}

int main() {
	char c = 10;

	std::string filename = "C:\\Users\\engel\\repos\\nt1100-analyser\\nt1100-analyser\\resources\\datasets\\drum1.asc";
	std::string file_string = "";
	try {
		std::ifstream file(filename);
		std::stringstream buffer;
		buffer << file.rdbuf();
		file_string = buffer.str();
		file.close();
	}
	catch (const std::exception& e) {
		std::cerr << "An error occurred loading file " << filename << ": " << e.what() << std::endl;
	}

	if (file_string != "") {
		std::cerr << "Successfully loaded file." << std::endl;

		for (size_t i = 0; i < 40; ++i) {
			std::cout << file_string[i] << " " << int(file_string[i]) << std::endl;
		}

		//find start of data
		size_t start = file_string.find("\nOPD\t");
		size_t end = file_string.find("\n", start + 1);
		std::cout << "Found: " << start << "-" << end << ": " << file_string.substr(start, end - start) << std::endl;
		std::string investigate_str = file_string.substr(end + 1, file_string.find_first_not_of("\t-0123456789.", end + 1)-end-1);
		std::cout << "Next line: " << investigate_str << std::endl;

		size_t datastart = end + 1;

		//count lines
		end++;
		size_t num_lines = std::count(file_string.begin() + end, file_string.end(), '\n');
		std::cout << "File contains " << num_lines << " data lines, ergo roughly " << num_lines * 3 << " doubles (" << num_lines * 3 * sizeof(double) / 1000000. << " Megabytes)" << std::endl;

		std::vector<double> data;
		data.reserve(num_lines * 3);
		//extract first triplet
		int count = 0;
		while (datastart < file_string.size()-1) {
			++count;
			if (count % 100000 == 0) {
				std::cout << datastart << "/" << file_string.size() << ", " << bad_lines << " bad lines." << std::endl;
				bad_lines = 0;
			}
			std::array<double, 3> temp_arr = extract_triplet(file_string, datastart);
			data.push_back(temp_arr[0]);
			data.push_back(temp_arr[1]);
			data.push_back(temp_arr[2]);
		}
		for (size_t i = 0; i < data.size() / 3; ++i) {
			std::cout << data[3*i] << " " << data[3*i + 1] << " " << data[3*i + 2] << std::endl;
		}
	}
}