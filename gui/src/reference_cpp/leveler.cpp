#include <leveler.hpp>
#include <imgui_bundle.hpp>
#include <string>
#include <cmath>
#include <iostream>

bool Leveler::fetchingP1 = false;
bool Leveler::fetchingP2 = false;
bool Leveler::fetchingP3 = false;
bool Leveler::transformationValid = false;

glm::vec3 Leveler::_p1;
glm::vec3 Leveler::_p2;
glm::vec3 Leveler::_p3;
glm::mat4 Leveler::transformation;
glm::mat4 Leveler::rotation;
float Leveler::modelZThatMapsToZero;

void Leveler::update_leveler() {
	ImGui::Begin("Leveler", nullptr);
	ImGui::Text("Status: ");
	if (fetchingP1) {
		ImGui::Text("Fetching P1");
	}
	else if (fetchingP2) {
		ImGui::Text("Fetching P2");
	}
	else if (fetchingP3) {
		ImGui::Text("Fetching P3");
	}
	else {
		ImGui::Text("Idle");
	}
	ImGui::Text("Plane identified: ");
	if (transformationValid) {
		//Write down plane equation here
	}
	else {
		ImGui::Text("N/A");
	}
	ImGui::Text("P1: ");
	ImGui::SameLine();
	std::string pstr = "(" + std::to_string(_p1.x) + ", " + std::to_string(_p1.y) + ", " + std::to_string(_p1.z) + ")";
	if (fetchingP1) {
		pstr = "";
	}
	ImGui::Text(pstr.c_str());
	ImGui::Text("P2: ");
	ImGui::SameLine();
	pstr = "(" + std::to_string(_p2.x) + ", " + std::to_string(_p2.y) + ", " + std::to_string(_p2.z) + ")";
	if (fetchingP1 || fetchingP2) {
		pstr = "";
	}
	ImGui::Text(pstr.c_str());
	ImGui::Text("P3: ");
	ImGui::SameLine();
	pstr = "(" + std::to_string(_p3.x) + ", " + std::to_string(_p3.y) + ", " + std::to_string(_p3.z) + ")";
	if (fetchingP1 || fetchingP2 || fetchingP3) {
		pstr = "";
	}
	ImGui::Text(pstr.c_str());
	ImGui::Text("");
	if (ImGui::Button("Place")) {
		transformationValid = false;
		fetchingP1 = true;
		fetchingP2 = false;
		fetchingP3 = false;
	}
	ImGui::End();
}


void Leveler::place(glm::vec3 p, glm::mat4 world2model) {
	if (fetchingP1) {
		_p1 = p;
		fetchingP1 = false;
		fetchingP2 = true;
	}
	else if (fetchingP2) {
		_p2 = p;
		fetchingP2 = false;
		fetchingP3 = true;
	}
	else if (fetchingP3) {
		_p3 = p;
		fetchingP3 = false;
		//TODO: calculate transformation here
		/*World transformation*/
		glm::vec3 n = glm::cross(_p2 - _p1, _p3 - _p1);
		n = std::copysign(1.f, n.z) * n;
		n = glm::normalize(n);
		glm::vec3 axis(n.y * n.z, -n.x * n.z, 0.f);
		axis = glm::normalize(axis);
		glm::vec3 xy(n.x, n.y, 0.f);
		xy = glm::normalize(xy);
		float phi = 3.14159265f * 0.5f - std::acos(xy.x * n.x + xy.y * n.y);
		//float phi = 0.5 * 3.141592 - std::acos(n.x * n.x + n.y * n.y);
		glm::mat4 unity(1.f);
		glm::mat4 translation = glm::translate(unity, -(_p1 + _p2 + _p3)/3.f);
		glm::mat4 rot = glm::rotate(unity, phi, axis);
		glm::mat4 translation2 = glm::translate(unity, glm::vec3(0.f, 0.f, -_p1.z));
		transformation = translation2 * glm::inverse(translation) * rot * translation;
		/*Model transformation*/
		glm::vec4 m4_p1 = world2model * glm::vec4(_p1, 1.);
		glm::vec4 m4_p2 = world2model * glm::vec4(_p2, 1.);
		glm::vec4 m4_p3 = world2model * glm::vec4(_p3, 1.);
		glm::vec3 m_p1(m4_p1.x, m4_p1.y, m4_p1.z);
		glm::vec3 m_p2(m4_p2.x, m4_p2.y, m4_p2.z);
		glm::vec3 m_p3(m4_p3.x, m4_p3.y, m4_p3.z);
		glm::vec3 m_n = glm::cross(m_p2 - m_p1, m_p3 - m_p1);
		m_n *= std::copysign(1., m_n.z);
		m_n = glm::normalize(m_n);
		glm::vec3 m_axis(m_n.y * m_n.z, -m_n.x * m_n.z, 0.f);
		m_axis = glm::normalize(m_axis);
		glm::vec3 m_xy(m_n.x, m_n.y, 0.f);
		m_xy = glm::normalize(m_xy);
		float m_phi = 3.14159265f * 0.5f - std::acos(m_xy.x * m_n.x + m_xy.y * m_n.y);
		rotation = glm::rotate(unity, m_phi, m_axis);
		/*Calculate model z that is to map to zero deformation (for regauging model)*/
		modelZThatMapsToZero = (m_p1.z + m_p2.z + m_p3.z) / 3.;
		transformationValid = true;
	}
}