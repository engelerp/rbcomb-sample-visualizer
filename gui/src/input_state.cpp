#include <input_state.hpp>
#include <iostream>
#include <imgui_bundle.hpp>

bool InputState::mwheel_pressed = false;
bool InputState::lmb_pressed = false;
bool InputState::lmb_click = false;
bool InputState::rmb_pressed = false;
bool InputState::esc_click = false;
bool InputState::space_pressed = false;
float InputState::mpos_x = 0.f;
float InputState::mpos_y = 0.f;
unsigned InputState::umpos_x = 0;
unsigned InputState::umpos_y = 0;
float InputState::mmot_x = 0.f;
float InputState::mmot_y = 0.f;
float InputState::mwheelmot_y = 0.f;
bool InputState::should_quit = false;
bool InputState::b_click = false;
bool InputState::s_click = false;
bool InputState::t_click = false;
bool InputState::g_click = false;
bool InputState::f1_click = false;
bool InputState::f2_click = false;
bool InputState::f3_click = false;

void InputState::update(const unsigned width, const unsigned height) {
	//reset relative data
	mmot_x = 0.f;
	mmot_y = 0.f;
	mwheelmot_y = 0.f;
	lmb_click = false;
	esc_click = false;
	b_click = false;
	s_click = false;
	t_click = false;
	g_click = false;
	f1_click = false;
	f2_click = false;
	f3_click = false;

	SDL_Event event;
	while (SDL_PollEvent(&event)) {
		/*Hand event off to ImGui*/
		ImGui_ImplSDL2_ProcessEvent(&event);
		if (ImGui::GetIO().WantCaptureMouse) {
			continue;
		}
		switch (event.type) {
		case SDL_QUIT:
			should_quit = true;
			break;
		case SDL_MOUSEMOTION:
			mpos_x = event.motion.x / static_cast<float>(width);
			mpos_y = 1. - event.motion.y / static_cast<float>(height);
			umpos_x = event.motion.x;
			umpos_y = event.motion.y;
			mmot_x = event.motion.xrel / static_cast<float>(width);
			mmot_y = -event.motion.yrel / static_cast<float>(height);
#ifndef NDEBUG
			std::cout << "Mouse Motion: "
				<< "(" << event.motion.x << ", " << event.motion.y << "), "
				<< "(" << event.motion.xrel << ", " << event.motion.yrel << "), "
				<< event.motion.state << std::endl;
#endif
			break;
		case SDL_MOUSEBUTTONDOWN:
			switch (event.button.button) {
			case SDL_BUTTON_LEFT:
#ifndef NDEBUG
				std::cout << "LMB Pressed" << std::endl << std::flush;
#endif
				lmb_pressed = true;
				lmb_click = true;
				break;
			case SDL_BUTTON_RIGHT:
#ifndef NDEBUG
				std::cout << "RMB Pressed" << std::endl << std::flush;
#endif
				rmb_pressed = true;
				break;
			case SDL_BUTTON_MIDDLE:
#ifndef NDEBUG
				std::cout << "MWHEEL Pressed" << std::endl << std::flush;
#endif
				mwheel_pressed = true;
				break;
			default:
				break;
			}
#ifndef NDEBUG
			std::cout << "Mousebutton Down: "
				<< "(" << event.button.x << ", " << event.button.y << "), "
				<< event.button.state << ", "
				<< event.button.which << std::endl;
#endif
			break;
		case SDL_MOUSEBUTTONUP:
			switch (event.button.button) {
			case SDL_BUTTON_LEFT:
#ifndef NDEBUG
				std::cout << "LMB Released" << std::endl << std::flush;
#endif
				lmb_pressed = false;
				break;
			case SDL_BUTTON_RIGHT:
#ifndef NDEBUG
				std::cout << "RMB Released" << std::endl << std::flush;
#endif
				rmb_pressed = false;
				break;
			case SDL_BUTTON_MIDDLE:
#ifndef NDEBUG
				std::cout << "MWHEEL Released" << std::endl << std::flush;
#endif
				mwheel_pressed = false;
				break;
			default:
				break;
			}
#ifndef NDEBUG
			std::cout << "Mousebutton Up: "
				<< "(" << event.button.x << ", " << event.button.y << "), "
				<< event.button.state << ", "
				<< event.button.which << std::endl;
#endif
			break;
		case SDL_MOUSEWHEEL:
#ifndef NDEBUG
			std::cout << "Mousewheel Scrolled: "
				<< event.wheel.y << std::endl;
#endif
			mwheelmot_y = static_cast<float>(event.wheel.y);
			break;
		case SDL_KEYDOWN:
#ifndef NDEBUG
			std::cout << "Keypress: "
				<< static_cast<int>(event.key.keysym.sym) << std::endl;
#endif
			if (event.key.keysym.sym == SDLK_ESCAPE) {
				esc_click = true;
			}
			if (event.key.keysym.sym == SDLK_SPACE) {
				space_pressed = true;
			}
			if (event.key.keysym.sym == SDLK_b) {
				b_click = true;
			}
			if (event.key.keysym.sym == SDLK_g) {
				g_click = true;
			}
			if (event.key.keysym.sym == SDLK_s) {
				s_click = true;
			}
			if (event.key.keysym.sym == SDLK_t) {
				t_click = true;
			}
			if (event.key.keysym.sym == SDLK_F1) {
				f1_click = true;
			}
			if (event.key.keysym.sym == SDLK_F2) {
				f2_click = true;
			}
			if (event.key.keysym.sym == SDLK_F3) {
				f3_click = true;
			}
			break;
		case SDL_KEYUP:
			if (event.key.keysym.sym == SDLK_SPACE) {
				space_pressed = false;
			}
			break;
		default:
			break;
		}
	}
}
