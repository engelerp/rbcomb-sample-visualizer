#include <layer.hpp>
#include <iostream>
#include <fstream>
#include <sstream>
#include <paths.hpp>
#include <shader.hpp>
#include <metadata_manager.hpp>
#include <material.hpp>
#include <INIReader.h>

Layer::Layer(std::string name, std::string filename, float zOffset, Material mat): _name(name), _zOffset(zOffset) {
    //read data from file into vertex buffer, element buffer
    std::stringstream buffer;
    try {
		std::ifstream file(filename, std::ios_base::in);
		buffer << file.rdbuf();
		file.close();
	}
    catch(const std::exception e){
        std::cerr << "An error occurred loading file " << filename << ": " << e.what() << std::endl;
        return;
    }

    char c;
    float x;
    unsigned e;
    int currentObjectIndex = -1;
    int red = 0;
    int green = 0;
    size_t running_vertex_index = 0;
    try{
        while(buffer){
            buffer >> c;
            if(c == 'v'){
                VertexData vd;
                buffer >> vd.vx;
                buffer >> vd.vy;
                buffer >> x; //don't push z
                vd.cr = float(red)/255.;
                vd.cg = float(green)/255.;
                vd.io = float(currentObjectIndex);
                vd.is = float(0);
                _objectDataFlat.push_back(vd);
                ++running_vertex_index;
            }
            else if(c == 'f'){
                buffer >> e;
                _elementIndices.push_back(e);
                buffer >> e;
                _elementIndices.push_back(e);
                buffer >> e;
                _elementIndices.push_back(e);
            }
            else if(c == 'o'){//next object starts here
                buffer >> e;
                currentObjectIndex += 1;
                red+=2;
                if(red >= 256){
                    green+=5;
                    red = 0;
                }
                _colorToObjectIndex[red + 2560*green] = currentObjectIndex;
                if(currentObjectIndex != 0){
                    _objectIndexRanges[currentObjectIndex - 1].second = running_vertex_index;
                }
                _objectIndexRanges.push_back({running_vertex_index, 0.});
            }
            else{
                std::cerr << "Encountered unknown directive " << c << " while parsing file " << filename << std::endl;
            }
        }
        //end index of last object yet to be placed
        _objectIndexRanges[_objectIndexRanges.size()-1].second = _objectDataFlat.size();
    }
    catch(const std::exception e){
        std::cerr << "An error occurred parsing file " << filename << ": " << e.what() << std::endl;
    }

    //generate buffers
    glGenBuffers(1, &_vbo);
    glGenBuffers(1, &_ebo);
    glGenVertexArrays(1, &_vao);
    
    //Upload EBO data
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned) * _elementIndices.size(), _elementIndices.data(), GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    
    //Upload VBO data
    glBindBuffer(GL_ARRAY_BUFFER, _vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float)*6*_objectDataFlat.size(), _objectDataFlat.data(), GL_STATIC_DRAW);

    //Setup VAO
    glBindVertexArray(_vao);
    glBindBuffer(GL_ARRAY_BUFFER, _vbo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ebo);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 6*sizeof(float), (void*)(0));
	glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 6*sizeof(float), (void*)(2 * sizeof(float)));
	glEnableVertexAttribArray(1);
    glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, 6*sizeof(float), (void*)(4 * sizeof(float)));
	glEnableVertexAttribArray(2);
    glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE, 6*sizeof(float), (void*)(5 * sizeof(float)));
	glEnableVertexAttribArray(3);
    glBindVertexArray(0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    //setup shader
    INIReader inireader("config.ini");
    std::string vertFile = inireader.GetString("paths", "pathShaders", "NONE") + std::string("objectShader.vert");
    std::string fragFile = inireader.GetString("paths", "pathShaders", "NONE") + std::string("objectShader.frag");
    _shader = Shader(vertFile.c_str(), fragFile.c_str());
    _shader.use();
    _shader.setFloat("z_offset", _zOffset);
    _shader.setFloat("selected_index", -1.f);
    _shader.setVec4("color_selected", glm::vec4(1.f,0.078f,0.576f,1.f));
    _shader.setInt("drawing_mode", 0);
    _shader.setVec4("color_normal", mat.color_normal);
    _shader.setVec4("color_good", mat.color_good);
    _shader.setVec4("color_bad", mat.color_bad);
    _shader.setVec4("color_sketch", mat.color_sketch);
    _shader.setFloat("opacity", 1.f);
    _shader.setBool("mirror", false);
    _shader.unuse();

    _framebuffer = Framebuffer();
}

void Layer::draw(glm::mat4 projection, glm::mat4 view, glm::mat4 model, int drawing_mode, int selected_index, float opacity){
    _shader.use();
    _shader.setInt("drawing_mode", drawing_mode);
    _shader.setMat4("projection", projection);
    _shader.setMat4("view", view);
    _shader.setMat4("model", model);
    _shader.setFloat("z_offset", _zOffset);
    _shader.setFloat("selected_index", float(selected_index));
    _shader.setFloat("opacity", opacity);
    glBindVertexArray(_vao);
    glBindBuffer(GL_ARRAY_BUFFER, _vbo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ebo);
    glDrawElements(GL_TRIANGLES, _elementIndices.size(), GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    _shader.unuse();
}

int Layer::selectObject(unsigned x, unsigned y, glm::mat4 projection, glm::mat4 view, glm::mat4 model) {
  _framebuffer.bind();

  glClearColor(0.f, 0.f, 1.f, 1.0f); //selection colors have no blue
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  draw(projection, view, model, 1, -1, 1.f);

  auto color = _framebuffer.getPixel(x, y);

#ifndef NDEBUG
  std::cout << "color float: (" << color[0] << ", " << color[1] << ", " << color[2] << ", " << color[3] << ")" << std::endl;
  std::cout << "color int: (" << color[0]*255 << ", " << color[1]*255 << ", " << color[2]*255 << ", " << color[3]*255 << ")" << std::endl;
#endif

  _framebuffer.unbind();

  if (color[2] > 1.e-5) {
    return -1; //mouse above background
  }
  else {
    int index = -1;
    try {
      int color_function = int(((color[0] + 2560 * color[1]) * 255.f) + 0.2);
#ifndef NDEBUG
      std::cout << "color function: " << color_function << std::endl;
#endif
      index = _colorToObjectIndex[color_function];
    }
    catch(...) {
      index = -1;
    }
    return index;
  }
}

void Layer::setObjectState(int objectIndex, int state) {
  size_t start = _objectIndexRanges[objectIndex].first;
  size_t end = _objectIndexRanges[objectIndex].second;
  //update local data
  for (size_t i = start; i < end; ++i) {
    _objectDataFlat[i].is = static_cast<float>(state);
  }
  //re-upload relevant portion to GPU
  glBindBuffer(GL_ARRAY_BUFFER, _vbo);
  glBufferSubData(GL_ARRAY_BUFFER, sizeof(float) * 6 * start, (sizeof(float) * 6) * (end - start), _objectDataFlat.data() + start);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void Layer::setMirrorState(bool mirror) {
  _shader.use();
  _shader.setBool("mirror", mirror);
  _shader.unuse();
}

bool Layer::setAllObjectStates(std::vector<ObjectMetadata> data) {
  if (data.size() != numObjects()) {
    std::cerr << "Size mismatch in Object States Update: Loaded " << data.size() << ", expected " << numObjects() << std::endl;
    return false;
  }
  for (size_t objectIndex = 0; objectIndex < data.size(); ++objectIndex) {
    int state = data[objectIndex].status;
    size_t start = _objectIndexRanges[objectIndex].first;
    size_t end = _objectIndexRanges[objectIndex].second;
    //update local data
    for (size_t i = start; i < end; ++i) {
      _objectDataFlat[i].is = static_cast<float>(state);
    }
  }
  //re-upload relevant portion to GPU
  glBindBuffer(GL_ARRAY_BUFFER, _vbo);
  glBufferSubData(GL_ARRAY_BUFFER, 0, (sizeof(float) * 6) * _objectDataFlat.size(), _objectDataFlat.data());
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  return true;
}

std::string Layer::name() const {
  return _name;
}

int Layer::numObjects() const {
  return _objectIndexRanges.size();
}