#include <glad/glad.h>
#include <SDL.h>
#include <wave_handler.hpp>
#include <infrastructure.hpp>
#include <iostream>

#define WIDTH 2048
#define HEIGHT 1024

int main(int argc, char** argv){
	Infrastructure infra;
	infra.init("Waves", WIDTH, HEIGHT);

	WaveHandler waves(WIDTH, HEIGHT, 0.2f, "C:\\Users\\engel\\VS_Projects\\FocusTerra\\framebuffer-testing\\shaders\\");
	waves.initialize();

	bool is_running = true;
	float time = 0.f;
	float dt = 0.016f;
	SDL_Event event;
	while (is_running) {
		/*
		inputHandler.fetch_events(toolbox);
		inputHandler.sanitize_events(toolbox); //sort and assign events
		
		guiHandler.update(toolbox);

		blockchain.update(toolbox);

		drawingManager.update(toolbox);
		*/

		




		//this will go
		while (SDL_PollEvent(&event)) {
			if (event.type == SDL_QUIT) {
				is_running = false;
			}
		}

		int w_width, w_height;
		SDL_GetWindowSize(infra.window(), &w_width, &w_height);


		for (size_t i = 0; i < 2; ++i) {
			waves.step(time, dt, 0.3f, 10.f);
		}

		waves.render(w_width, w_height);

		SDL_GL_SwapWindow(infra.window());

		std::cout << time << std::endl;
		
		/*std::cout << "Error Code: " << glGetError() << std::endl;
		std::cout << "GL_NO_ERROR: " << GL_NO_ERROR << std::endl;
		std::cout << "GL_INVALID_ENUM: " << GL_INVALID_ENUM << std::endl;
		std::cout << "GL_INVALID_VALUE: " << GL_INVALID_VALUE << std::endl;
		std::cout << "GL_INVALID_OPERATION: " << GL_INVALID_OPERATION << std::endl;
		std::cout << "GL_INVALID_FRAMEBUFFER_OPERATION: " << GL_INVALID_FRAMEBUFFER_OPERATION << std::endl;
		std::cout << "GL_OUT_OF_MEMORY: " << GL_OUT_OF_MEMORY << std::endl;
		
		GLint num_vti;
		glGetIntegerv(GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS, &num_vti);
		std::cout << num_vti << std::endl;*/
	}

	return 0;
}