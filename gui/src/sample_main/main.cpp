#include <heightmap.hpp>
#include <resource_manager.h>
#include <infrastructure.hpp>
#include <memory>
#include <iostream>
#include <chrono>
#include <input_state.hpp>
#include <camera_arcball.hpp>
#include <imgui_bundle.hpp>
#include <utility.hpp>
#include <loader.hpp>
#include <leveler.hpp>
#include <batch_loader.hpp>

#include <screendefs.hpp>

int main(int argc, char** argv) {
	/*Init Window and OpenGL Context*/
	Infrastructure infra;
	infra.init("NT1100 Analyzer", WIDTH, HEIGHT);
	glEnable(GL_DEPTH_TEST);

	/*Init GUI*/
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImPlot::CreateContext();
	ImGui::StyleColorsDark();
	ImGui_ImplSDL2_InitForOpenGL(infra.window(), infra.context());
	const char* glsl_version = "#version 330";
	ImGui_ImplOpenGL3_Init(glsl_version);


	glm::vec3 lookAt(0., 0., 0.);
	glm::vec3 eye(1.5, 1.5, 0.8);
	glm::vec3 upVector(0., 0., 1.);
	Camera camera (eye, lookAt, upVector);

	//global scale of heightmaps
	float scale = 0.25;

	std::vector<std::shared_ptr<Heightmap>> heightmaps; //heightmaps
	std::vector<glm::mat4> models; //model transformations
	std::vector<glm::vec3> positions; //model positions

	std::cout << "Loading Heightmap" << std::endl << std::flush;
	heightmaps.push_back(ResourceManager::fetch_heightmap(std::string(HEIGHTMAP_LOC)+"drum1.asc"));
	float zRangeUmReference = heightmaps.back()->zRangeUm();
	positions.push_back(glm::vec3(0.0, 0.0, 0));
	models.push_back(glm::translate(scale_z(heightmaps.back()->minZ(), scale), positions.back()));
	std::cout << "Loading complete" << std::endl << std::flush;

	std::cout << "Error Code: " << glGetError() << std::endl;
	std::cout << "GL_NO_ERROR: " << GL_NO_ERROR << std::endl;
	std::cout << "GL_INVALID_ENUM: " << GL_INVALID_ENUM << std::endl;
	std::cout << "GL_INVALID_VALUE: " << GL_INVALID_VALUE << std::endl;
	std::cout << "GL_INVALID_OPERATION: " << GL_INVALID_OPERATION << std::endl;
	std::cout << "GL_INVALID_FRAMEBUFFER_OPERATION: " << GL_INVALID_FRAMEBUFFER_OPERATION << std::endl;
	std::cout << "GL_OUT_OF_MEMORY: " << GL_OUT_OF_MEMORY << std::endl;

	size_t count = 0;
	auto start = std::chrono::high_resolution_clock::now();
	while (!InputState::should_quit) {
		++count; //number of frames processed

		/*New Gui Frame*/
		ImGui_ImplOpenGL3_NewFrame();
		ImGui_ImplSDL2_NewFrame(infra.window());
		ImGui::NewFrame();
		//ImGui::ShowDemoWindow(); //see imgui_demo.cpp, that's where this comes from

		/*Process Input*/
		InputState::update(WIDTH, HEIGHT);
		if (InputState::rmb_pressed) {
			camera.ProcessMouseRotation(-InputState::mmot_x, InputState::mmot_y);
		}
		else if (InputState::mwheel_pressed) {
			camera.ProcessMousePan(-InputState::mmot_x, -InputState::mmot_y);
		}
		else if (InputState::mwheelmot_y != 0.f) {
			camera.ProcessMouseZoom(InputState::mwheelmot_y);
		}

		/*Load new heightmaps*/
		if (Loader::load_update()) {
			float target_x = heightmaps.back()->maxX()+0.02;
			float target_y = heightmaps.back()->minY();
			heightmaps.push_back(Loader::getHeightmap());
			positions.push_back(glm::vec3(target_x - heightmaps.back()->minX(), target_y - heightmaps.back()->minY(), 0) + positions.back());
			models.push_back(glm::translate(scale_z(heightmaps.back()->minZ(), heightmaps.back()->zRangeUm() * scale / zRangeUmReference), positions.back()));
		}
		/*BatchLoad new heightmaps*/
		if (BatchLoader::load_update()) {
			for (auto h : BatchLoader::getHeightmaps()) {
				float target_x = heightmaps.back()->maxX() + 0.02;
				float target_y = heightmaps.back()->minY();
				heightmaps.push_back(h);
				positions.push_back(glm::vec3(target_x - heightmaps.back()->minX(), target_y - heightmaps.back()->minY(), 0) + positions.back());
				models.push_back(glm::translate(scale_z(heightmaps.back()->minZ(), heightmaps.back()->zRangeUm() * scale / zRangeUmReference), positions.back()));
			}
		}

		/*Leveler Control Window*/
		Leveler::update_leveler();


    /*Plotting*/
		for (auto heightmap : heightmaps) {
			heightmap->updatePlotWindow();
		}
		/*
    ImGui::Begin("Heightplot", nullptr, ImGuiWindowFlags_AlwaysAutoResize);
		ImGui::Text("Some Long Text to make sure this Window has a certain size! It's necessary.");
    static float* x_data = new float[25];
    static float* y_data = new float[25];
    for(size_t i = 0; i < 25; ++i){
			x_data[i] = static_cast<float>(i)*3.1415 / 25;
			y_data[i] = std::sin(x_data[i]);
    }
    if(ImPlot::BeginPlot("Heightplot")){
			ImPlot::SetupAxes("Position [mm]", "Height [um]");
			ImPlot::SetNextLineStyle(ImVec4(0.f,0.5f,1.f,1.f));
			ImPlot::PlotLine("Sinus", x_data, y_data, 25);
			ImPlot::EndPlot();
    }
    ImGui::End();
		*/

		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		/*Update projection, view, model*/
		glm::mat4 projection = glm::perspective(glm::radians(45.f), static_cast<float>(WIDTH) / static_cast<float>(HEIGHT), 0.1f, 100.0f);
		glm::mat4 view = camera.GetViewMatrix();




		/*Check if we want to get the height*/
		float height_measured = NOT_ON_SURFACE;
		size_t hmap_hovered_index = 99999;
		if (InputState::lmb_pressed && !(ImGui::GetIO().WantCaptureMouse)) {
			for (size_t i = 0; i < heightmaps.size(); ++i) {
				height_measured = heightmaps[i]->getSurfaceHeight(InputState::umpos_x, HEIGHT - InputState::umpos_y, projection, view, models[i]);
				if (height_measured != NOT_ON_SURFACE) {
					hmap_hovered_index = i;
					break;
				}
			}
			ImGui::BeginTooltip();
			std::string tool_tip_str = "Height (";
			tool_tip_str += (height_measured != NOT_ON_SURFACE) ? std::to_string(hmap_hovered_index) + "): " : "-1): ";
			tool_tip_str += (height_measured != NOT_ON_SURFACE) ? std::to_string(height_measured) + " um" : "N/A";
			ImGui::Text(tool_tip_str.c_str());
			ImGui::EndTooltip();
		}



		/*Find x-y world coordinates corresponding to mouse coordinates*/
		if (Leveler::fetchingP1 || Leveler::fetchingP2 || Leveler::fetchingP3) {
			//get highlighting point
			glm::vec2 mowopos = camera.GetWorldXyFromMouse(InputState::mpos_x, InputState::mpos_y);
			if (InputState::lmb_click && height_measured != NOT_ON_SURFACE) {//Have new point
				//transform coordinates from world to model
				glm::vec4 p_world(mowopos[0], mowopos[1], 0.f, 1.f);
				glm::vec4 p_model = glm::inverse(models[hmap_hovered_index]) * p_world;
				p_model[2] = heightmaps[hmap_hovered_index]->getAverageSurfaceHeight(glm::vec2(p_model[0], p_model[1]));
				p_world = models[hmap_hovered_index] * p_model;
				Leveler::place(glm::vec3(p_world[0], p_world[1], p_world[2]), glm::inverse(models[hmap_hovered_index]));
				if (Leveler::transformationValid) {
					models[hmap_hovered_index] = Leveler::transformation * models[hmap_hovered_index];
					//heightmaps[hmap_hovered_index]->setZRangeUmOffset(height_measured);
					heightmaps[hmap_hovered_index]->setZRangeUmFromModelZ(Leveler::modelZThatMapsToZero);
					heightmaps[hmap_hovered_index]->calculateNewZRange(Leveler::rotation);
					heightmaps[hmap_hovered_index]->setModelspaceRot(Leveler::rotation);
				}
			}
			for (auto heightmap : heightmaps) { //highlight selection
				heightmap->enableHighlighting(mowopos);
			}
		}
		else {
			for (auto heightmap : heightmaps) { //don't highlight
				heightmap->disableHighlighting();
			}
		}





		/*Draw here*/
		//heightmap->disableLighting();
		for (size_t i = 0; i < heightmaps.size(); ++i) {
			heightmaps[i]->draw(projection, view, models[i], camera.GetEye());
		}





		/*End GUI Frame*/
		ImGui::Render();
		glViewport(0, 0, WIDTH, HEIGHT);
		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

		SDL_GL_SwapWindow(infra.window());


		auto end = std::chrono::high_resolution_clock::now();
		auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
		if (duration > 1000) {
			std::cout << "FPS: " << count << std::endl << std::flush;
			count = 0;
			start = std::chrono::high_resolution_clock::now();
		}
	}

	return 0;
}
