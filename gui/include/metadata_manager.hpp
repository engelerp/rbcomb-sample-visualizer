#pragma once
#include <string>
#include <layer.hpp>
#include <map>

struct ObjectMetadata {
	int status;
	std::string info = "\tNot applicable";
};

class MetadataManager {
public:
	static bool load(std::string filename, std::string layername);
	static bool save(std::string filename, std::string layername);

	static bool addLayer(const Layer& layer);
	static void markBottomMetal(std::string layername);

	static ObjectMetadata& getData(std::string layername, int objectIndex);
	static std::vector<ObjectMetadata> getLayerData(std::string layername);

private:
	static std::map<std::string, std::vector<ObjectMetadata>> _metadataMap; // layer_name -> metadata_vector
	static std::string _bottomMetalLayername; //name of layer marked bottom metal
};