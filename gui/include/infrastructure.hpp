#ifndef INFRASTRUCTURE_HPP_INCLUDED
#define INFRASTRUCTURE_HPP_INCLUDED
#include <glad/glad.h>
//#include <SDL2/SDL.h>
#include <SDL.h>
#include <string>

class Infrastructure{
public:
  Infrastructure(SDL_Window* w, SDL_Renderer* r, SDL_GLContext c)
    : window_(w), renderer_(r), context_(c) {}
  Infrastructure() = default;
  Infrastructure(const Infrastructure&)=default;
  ~Infrastructure()=default;

  SDL_Window* window() const {return window_;}
  SDL_Renderer* renderer() const {return renderer_;}
  SDL_GLContext context() const {return context_;}

  bool init(const std::string name, const int width, const int height);

  void destroy(){
    SDL_GL_DeleteContext(context_);
    SDL_DestroyRenderer(renderer_);
    SDL_DestroyWindow(window_);
  }

  void quit(){
    destroy();
    SDL_Quit();
  }

private:
  SDL_Window* window_;
  SDL_Renderer* renderer_;
  SDL_GLContext context_;
};

#endif
