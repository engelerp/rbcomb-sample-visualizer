#pragma once
#include <glad/glad.h>
#include <array>

class Framebuffer {
public:
	Framebuffer();

	void bind();
	void unbind();
	std::array<float, 4> getPixel(unsigned x, unsigned y);

private:
	GLuint _fbo;
	GLuint _texture;
	GLuint _rbo;
};