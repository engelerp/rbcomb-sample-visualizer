#pragma once

struct VisibilityManager {
	static bool enableTop;
	static bool enableBottom;

	static float opacityMixer;

	static float topOpacity();
	static float bottomOpacity();
	/*
		Opacity Mixer:
		0.f  - top transparent
		1.f  - bottom transparent
		0.5f - nothing transparent

		alphaTop = std::max( 2.f * opacityMixer, 1.f );
		alphaBottom = std::max( 2.f * ( 1.f - opacityMixer ) );
	*/
};