#ifndef SHADER_HPP_INCLUDED
#define SHADER_HPP_INCLUDED

#include <glad/glad.h>

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

class Shader{
public:
  Shader();
  Shader(const char* vertexPath, const char* fragmentPath);
  Shader(const Shader& other) = default;
  Shader& operator=(const Shader& other) = default;
  ~Shader() = default;
  void clean_up(){glDeleteShader(ID);}
  // use/activate the shader
  void use();
  void unuse();
  // check identity of shader
  bool is(const std::string vertexPath, const std::string fragmentPath) const;
  // utility uniform functions
  void setBool(const std::string &name, bool value) const;
  void setInt(const std::string &name, int value) const;
  void setFloat(const std::string &name, float value) const;
  void setVec2(const std::string &name, glm::vec2 vec) const;
  void setVec3(const std::string &name, glm::vec3 vec) const;
  void setVec4(const std::string &name, glm::vec4 vec) const;
  void setMat4(const std::string &name, glm::mat4 mat) const;

  //TODO: make this private
  unsigned int ID; //shader program ID

private:
  std::string _vertexPath;
  std::string _fragmentPath;
};

#endif
