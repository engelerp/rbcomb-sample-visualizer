#pragma once
#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#define _USE_MATH_DEFINES
#include <math.h>
#include <screendefs.hpp>
#include <iostream>

class Camera {
public:
	Camera() = default;

	Camera(glm::vec3 eye, glm::vec3 lookat, glm::vec3 upVector)
		: m_eye(std::move(eye)),
		m_lookAt(std::move(lookat)),
		m_upVector(std::move(upVector))
	{
		UpdateViewMatrix();
	}

	glm::mat4 GetViewMatrix() const { return m_viewMatrix; }
	glm::vec3 GetEye() const { return m_eye; }
	glm::vec3 GetUpVector() const { return m_upVector; }
	glm::vec3 GetLookAt() const { return m_lookAt; }

	glm::vec3 GetViewDir() const { return normalize(m_lookAt-m_eye); }
	glm::vec3 GetRightVector() const { return glm::normalize(glm::cross(m_lookAt - m_eye, m_upVector)); }

	glm::vec2 GetWorldXyFromMouse(float mx, float my) const {
		//calculate point in center of screen
		glm::vec3 p_eye = m_eye;
		glm::vec3 p_sCenter = m_eye + 0.1f * GetViewDir();
		glm::vec3 curr_up = normalize(glm::cross(GetRightVector(), GetViewDir()));
		//extent of screen in width and height
		//0.1 away from eye, spans 45� (see call to glm::perspective in main.cpp)
		//float s_extent_h = 0.2 * std::tan(3.14159265 * 45 / 180.);
		float s_extent_h = 0.2f * std::tan(3.14159265 * 45 / 180. / 2.);
		float s_extent_w = WIDTH / static_cast<float>(HEIGHT) * s_extent_h;
		glm::vec3 p_mouse = p_sCenter + (mx - 0.5f) * s_extent_w * GetRightVector() + (my - 0.5f) * s_extent_h * curr_up; //TODO: Potentially needs (1-my) instead of my
		glm::vec3 r = glm::normalize(p_mouse - p_eye);
		if (std::abs(r.z) > 0.00001) {
			float t = -p_mouse.z / r.z;
			glm::vec3 res_p = p_mouse + t * r;
			return glm::vec2(res_p.x, res_p.y);
		}
		else {
			return glm::vec2(std::copysign(r.x, 999999.), std::copysign(r.y, 999999.));
		}
	
	
	}

	void SetCameraView(glm::vec3 eye, glm::vec3 lookat, glm::vec3 up) {
		m_eye = std::move(eye);
		m_lookAt = std::move(lookat);
		m_upVector = std::move(up);
		UpdateViewMatrix();
	}

	void UpdateViewMatrix() {
		m_viewMatrix = glm::lookAt(m_eye, m_lookAt, m_upVector);
	}

	void ProcessMouseRotation(float xrel, float yrel) {
		glm::vec4 position(m_eye.x, m_eye.y, m_eye.z, 1.f);
		glm::vec4 pivot(m_lookAt.x, m_lookAt.y, m_lookAt.z, 1.f);
		
		float deltaAngleX = 10.;//2. * M_PI;
		float deltaAngleY = 10.;// 2. * M_PI;
		float xAngle = 2. * xrel * deltaAngleX;
		float yAngle = 2. * yrel * deltaAngleY;

		float cosAngle = glm::dot(GetViewDir(), glm::normalize(m_upVector));
		if(cosAngle * std::copysign(1.f, yAngle) > 0.99f){
			std::cout << "Camera Too Close To A Pole!" << std::endl << std::flush;
			yAngle = 0.f;
		}

		glm::mat4 rotationMatrixX(1.0f);
		rotationMatrixX = glm::rotate(rotationMatrixX, xAngle, m_upVector);
		position = rotationMatrixX * (position - pivot) + pivot;

		glm::mat4 rotationMatrixY(1.0f);
		rotationMatrixY = glm::rotate(rotationMatrixY, yAngle, GetRightVector());

		glm::vec3 finalPosition = rotationMatrixY * (position - pivot) + pivot;

		//update camera
		m_eye = finalPosition;
		UpdateViewMatrix();
	}

	void ProcessMousePan(float xrel, float yrel) {
		glm::vec4 position(m_eye.x, m_eye.y, m_eye.z, 1.f);
		glm::vec4 pivot(m_lookAt.x, m_lookAt.y, m_lookAt.z, 1.f);

		float xfactor = 1.3f * m_eye.z * xrel;
		float yfactor = 0.8f * m_eye.z * yrel;

		//glm::vec3 delta = xfactor * GetRightVector() + yfactor * glm::normalize(glm::vec3(m_lookAt.x - m_eye.x, m_lookAt.y - m_eye.y, 0.f));
		glm::vec3 delta = glm::vec3(xfactor, -yfactor, 0.);
		m_eye += delta;
		m_lookAt += delta;
		UpdateViewMatrix();
	}

	void ProcessMouseZoom(float yrel) {
		glm::vec3 negViewDirection = m_eye - m_lookAt;
		float zoomFactor = 0.9;
		if (yrel > 0) {
			m_eye = m_lookAt + zoomFactor * negViewDirection;
		}
		else if (yrel < 0) {
			m_eye = m_lookAt + (1.f / zoomFactor) * negViewDirection;
		}

		if (m_eye.z < 0.101f) {
			m_eye.z = 0.101f;
		}
		if (m_eye.z > 199.9f) {
			m_eye.z = 199.9f;
		}

		UpdateViewMatrix();
	}

	void MoveTo(glm::vec2 pos_new) {
		glm::vec3 translation{ pos_new.x - m_lookAt.x, pos_new.y - m_lookAt.y, 0.f };
		m_eye += translation;
		m_lookAt += translation;
		UpdateViewMatrix();
	}

private:
	glm::mat4 m_viewMatrix;
	glm::vec3 m_eye;
	glm::vec3 m_lookAt;
	glm::vec3 m_upVector;

	void _printCamera() const {
		std::cout << "Eye: (" << m_eye.x << ", " << m_eye.y << ", " << m_eye.z << "), " << "Target: (" << m_lookAt.x << ", " << m_lookAt.y << ", " << m_lookAt.z << ")" << std::endl;
	}
};