#pragma once
#include <memory>
#include <heightmap.hpp>
#include <shader.hpp>
#include <vector>
#include <colorscheme.hpp>

//#define SHADER_LOC "C:\\Users\\engel\\repos\\nt1100-analyser\\nt1100-analyser\\resources\\shaders\\"
//#define HEIGHTMAP_LOC "C:\\Users\\engel\\repos\\nt1100-analyser\\nt1100-analyser\\resources\\datasets\\"
//#define COLORSCHEME_LOC "C:\\Users\\engel\\repos\\nt1100-analyser\\nt1100-analyser\\resources\\colorschemes\\"
#define SHADER_LOC "C:\\Users\\Pascal\\repos\\nt1100-analyser\\resources\\shaders\\"
#define HEIGHTMAP_LOC "C:\\Users\\Pascal\\repos\\nt1100-analyser\\resources\\datasets\\"
#define COLORSCHEME_LOC "C:\\Users\\Pascal\\repos\\nt1100-analyser\\resources\\colorschemes\\"

class ResourceManager {
public:
	static std::shared_ptr<Heightmap> fetch_heightmap(std::string filename);
	static std::shared_ptr<Shader> fetch_shader(std::string vertexPath, std::string fragmentPath);
	static std::shared_ptr<Colorscheme> fetch_colorscheme(std::string filename);

private:
	static std::vector<std::shared_ptr<Heightmap>> _heightmaps;
	static std::vector<std::shared_ptr<Shader>> _shaders;
	static std::vector<std::shared_ptr<Colorscheme>> _colorschemes;
};