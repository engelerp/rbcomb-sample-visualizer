#pragma once
#include <heightmap.hpp>
#include <resource_manager.h>
#include <memory>
#include <utility.hpp>

class Loader {
public:
	static bool load_update();
	static std::shared_ptr<Heightmap> getHeightmap();

private:
	static std::shared_ptr<Heightmap> _heightmap;
	static char _buf[128];
};