#pragma once
#include <drawable.hpp>
#include <shader.hpp>
#include <glad/glad.h>
#include <SDL.h>
#include <colorscheme.hpp>
#include <framebuffer.hpp>

#include <string>
#include <vector>
#include <memory>
#include <utility>

#define NOT_ON_SURFACE -999999.

class Heightmap : public Drawable {
public:
	Heightmap(std::string); //load from file
	~Heightmap() = default;

	//check identity
	bool is(const std::string filename) const;

	//inherited methods
	virtual void draw(glm::mat4 projection, glm::mat4 view, glm::mat4 model, glm::vec3 camera_pos) override;
	virtual void upload() override;
	virtual void unload() override;

	//get deformation height in um at screen coordinate
	float getSurfaceHeight(unsigned x, unsigned y, glm::mat4 projection, glm::mat4 view, glm::mat4 model);
	float getAverageSurfaceHeight(glm::vec2 xy);

	void updatePlotWindow();

  float minZ() const;
	float minX() const;
	float maxX() const;
	float minY() const;
	float maxY() const;
	float zRangeUm() const;
	void setZRangeUmOffset(float);
	void setZRangeUmFromModelZ(float);

	//Shader control
	void enableLighting();
	void disableLighting();
	void enableHighlighting(glm::vec2);
	void disableHighlighting();

	//Leveling
	void calculateNewZRange(glm::mat4);
	void setModelspaceRot(glm::mat4);

private:
	void _plotHeightLineAlongX(const float y);
	void _plotHeightLineAlongY(const float x);


	//filename of origin
	std::string _filename;
	//data
	std::vector<double> _data; //raw data: x y z x y z ...
	double _minX, _maxX;
	double _minY, _maxY;
	double _minZ, _maxZ; //note that the (raw) z data seems to come in 100nm units, while x,y are in mm
	double _zRangeUm; //this we do in um (presumably)
	unsigned _Ny, _Nx; //number of points in y/x direction (length of a column/row)
	//render infrastructure
	GLuint _vao;
	GLuint _vbo;
	GLuint _ebo; //absolutely needs this
	std::shared_ptr<Shader> _shader_ptr;
	float _scale;
	GLuint _numElements;
	std::shared_ptr<Colorscheme> _colorscheme_ptr;
	std::shared_ptr<Framebuffer> _framebuffer_ptr;
  //plot infrastructure
  float _plotAlongX_y;
  float _plotAlongY_x;
  float _last_plotAlongX_y;
  float _last_plotAlongY_x;
  std::vector<float> _plotAlongX_data_x;
  std::vector<float> _plotAlongX_data_h;
  std::vector<float> _plotAlongY_data_y;
  std::vector<float> _plotAlongY_data_h;
	bool _plot_gets_focus; //should the plot window be focused
	bool _plot_gets_collapse; //plot window should collapse
	//highlighting infrastructure
	float _highlightingRadius;
	float _zRangeUmOffset;
	//leveling infrastructure
	glm::mat4 _modelSpaceRot;
};
