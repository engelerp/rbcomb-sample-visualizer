#pragma once
#include <heightmap.hpp>
#include <resource_manager.h>
#include <memory>
#include <utility.hpp>
#include <vector>

class BatchLoader {
public:
	static bool load_update();
	static std::vector<std::shared_ptr<Heightmap>> getHeightmaps();

private:
	static std::vector<std::shared_ptr<Heightmap>> _heightmaps;
	static char _buf[128];
	static char _basePath[128];
};