#pragma once
#include <string>
#include <glad/glad.h>
#include <vector>

class Colorscheme {
public:
	Colorscheme(std::string filename);

	bool is(const std::string filename) const;
	GLuint getTexture() const;

	float color2value(float r, float g, float b) const;

private:
	GLuint _texture;
	std::vector<float> _colors;

	std::string _filename;
};