#pragma once
#include <array>
#include <string>
#include <exception>
#include <limits>
#include <iostream>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#define BAD_Z_VALUE -999999.

class Nt1100_exception: std::exception {
public:
	Nt1100_exception(std::string);

	virtual const char* what() const override;

private:
	std::string _what;
};

std::array<double, 3> extract_triplet(const std::string& str_in, size_t& start);

/*i: column, j: row. unsigned for opengl compatibility*/
unsigned ij2index(unsigned i, unsigned j, unsigned Ny);

std::array<float, 3> operator+=(std::array<float, 3>&, const std::array<float, 3>&);

std::array<float, 3> operator-(std::array<float, 3>, const std::array<float, 3>&);

std::array<float, 3> cross(const std::array<float, 3>&, const std::array<float, 3>&);

std::array<float, 3> operator/=(std::array<float, 3>&, const float);

float length(const std::array<float, 3>&);

std::array<float, 3> normal(const std::array<float, 3>&, const std::array<float, 3>&, const std::array<float, 3>&);

glm::mat4 scale_z(float z0, float factor);







/*Templates: Need definition in header*/
template <class T>
double min(const T& container, size_t offset, size_t stride) {
	double min_val = std::numeric_limits<double>::max();
	size_t min_index = 0;
	for (size_t i = offset; i < container.size(); i+=stride) {
		if (min_val > container[i] && container[i] != BAD_Z_VALUE) {
			min_val = container[i];
			min_index = i;
		}
	}
	return min_val;
}

template <class T>
double max(const T& container, size_t offset, size_t stride) {
	double max_val = -(std::numeric_limits<double>::max()-10.);
	size_t max_index = 0;
	for (size_t i = offset; i < container.size(); i += stride) {
		if (max_val < container[i] && container[i] != BAD_Z_VALUE) {
			max_val = container[i];
			max_index = i;
		}
	}
	return max_val;
}
