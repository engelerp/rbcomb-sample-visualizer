#pragma once
#include <SDL.h>

class InputState {
public:
	static void update(const unsigned width, const unsigned height);

	//state of mouse buttons
	static bool mwheel_pressed;
	static bool lmb_pressed;
	static bool lmb_click;
	static bool rmb_pressed;

	//mouse position
	static float mpos_x;
	static float mpos_y;

	static unsigned umpos_x;
	static unsigned umpos_y;

	//relative mouse motion
	static float mmot_x;
	static float mmot_y;

	//relative mouse wheel motion
	static float mwheelmot_y;

	//quit event
	static bool should_quit;
};