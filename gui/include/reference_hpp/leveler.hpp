#pragma once
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/transform.hpp>

class Leveler {
public:
	static void update_leveler();
	static void place(glm::vec3, glm::mat4);

	static bool fetchingP1;
	static bool fetchingP2;
	static bool fetchingP3;

	static bool transformationValid;

	static glm::mat4 transformation; //total transformation, rotation and z translation
	static glm::mat4 rotation; //rotation part of transformation, around the origin, apply to _data to get new zrange

	static float modelZThatMapsToZero;

private:
	static glm::vec3 _p1;
	static glm::vec3 _p2;
	static glm::vec3 _p3;

};