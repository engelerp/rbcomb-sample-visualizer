#pragma once
#include <glm/glm.hpp>

class Drawable {
public:
	Drawable() = default;
	virtual ~Drawable() = default;

	virtual void draw(glm::mat4 projection, glm::mat4 view, glm::mat4 model, glm::vec3 camera_pos) = 0;
	virtual void upload() = 0;
	virtual void unload() = 0;
};