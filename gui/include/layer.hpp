#ifndef LAYER_HPP_INCLUDED
#define LAYER_HPP_INCLUDED
#include <glad/glad.h>
#include <glm/glm.hpp>
#include <string>
#include <vector>
#include <map>
#include <shader.hpp>
#include <framebuffer.hpp>
#include <material.hpp>

struct ObjectMetadata;

struct VertexData{
    float vx;
    float vy;
    float cr;
    float cg;
    float io;
    float is;
};

class Layer{
public:
    Layer(std::string name, std::string filename, float zOffset, Material mat);

    void draw(glm::mat4 projection, glm::mat4 view, glm::mat4 model, int drawing_mode, int selected_index, float opacity);

    int selectObject(unsigned x, unsigned y, glm::mat4 projection, glm::mat4 view, glm::mat4 model);

    void setObjectState(int objectIndex, int state);
    bool setAllObjectStates(std::vector<ObjectMetadata> data);
    void setMirrorState(bool mirror);

    std::string name() const;
    int numObjects() const;

private:
    std::string _name;
    std::vector<std::pair<size_t, size_t>> _objectIndexRanges; //contains _objectIndexRanges[k] = [i_k, i_k+N)
    std::vector<VertexData> _objectDataFlat; //generated from objectData, contiguous for upload to GPU
    std::vector<unsigned> _elementIndices;
    std::map<int, int> _colorToObjectIndex; //_colorToObjectIndex[red + 2560*green] = currentObjectIndex

    GLuint _vao, _vbo, _ebo;
    GLuint _selected;
    float _zOffset;

    Shader _shader;
    Framebuffer _framebuffer;
};

#endif