#pragma once
#include <string>
#include <glad/glad.h>
#include <glm/glm.hpp>
#include <shader.hpp>

class Overlay {
public:
	static void init();
	static void loadImage(std::string filename);

	static void draw(glm::mat4 projection, glm::mat4 view, glm::mat4 model);

	//controlled by gui
	static float radians;
	static glm::vec2 offset;
	static float scale;
	static float opacity;

private:
	static int _width, _height, _nrChannels;
	static unsigned char* _data;
	static GLuint _texture;
	static Shader _shader;
	static GLuint _vbo;
	static GLuint _vao;
	static GLuint _ebo;

	static bool _imageLoaded;
};