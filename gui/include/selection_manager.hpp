#pragma once
#include <string>

struct SelectionManager {
	//data of click-selected object
	static int selectedIndex;
	static std::string selectedLayerName;
	//data of hover-selected object
	static int hoveredIndex;
	static std::string hoveredLayerName;
};