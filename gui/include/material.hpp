#pragma once
#include <glm/glm.hpp>

struct Material {
	glm::vec4 color_normal;
	glm::vec4 color_good;
	glm::vec4 color_sketch;
	glm::vec4 color_bad;
};